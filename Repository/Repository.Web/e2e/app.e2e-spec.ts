import { RepositoryWebPage } from './app.po';

describe('repository-web App', function() {
  let page: RepositoryWebPage;

  beforeEach(() => {
    page = new RepositoryWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
