import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { LocalStorageService } from 'angular-2-local-storage';
import { AuthService } from './auth.service';

import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class FileService {

    serviceBase: string = 'http://localhost:19434';

    constructor(
        private http: Http,
        private authservice: AuthService,
        private localStorage: LocalStorageService
    ) { }

    _uploadFiledB(fileData): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/files/upload/dB', fileData, { headers: headers })
            .toPromise()
            .catch(this.authservice.handleError);
    }

    _uploadFileDrive(formData, parentId): Promise<Response> {

        //Solo para esta funcion se usan estos headers porque si no da problemas al mandarles los que tienen content type y application
        let headers = new Headers();
        if (this.authservice._authentication.isAuth) {
            var authData = JSON.parse(this.localStorage.get<any>('authorizationData'));
            headers = new Headers({ 'Authorization': 'Bearer ' + authData.token });
        }

        let options = new RequestOptions({ headers: headers });  

        return this.http.post(this.serviceBase + '/api/files/upload/drive/' + parentId, formData, options)
            .toPromise()
            .catch(this.authservice.handleError);
    }

    _updateFile(formData, parentId): Promise<Response> {

        //Solo para esta funcion se usan estos headers porque si no da problemas al mandarles los que tienen content type y application
        let headers = new Headers();
        if (this.authservice._authentication.isAuth) {
            var authData = JSON.parse(this.localStorage.get<any>('authorizationData'));
            headers = new Headers({ 'Authorization': 'Bearer ' + authData.token });
        }

        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.serviceBase + '/api/files/update/' + parentId, formData, options)
            .toPromise()
            .catch(this.authservice.handleError);
    }

    _getFiles(folderId): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.get(this.serviceBase + '/api/files/get/files/' + folderId, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _downloadFile(fileId): Promise<Response> { //No pasa response.json() as Response porque si no hay problemas a la hora de obtener los datos del cuerpo

        var headers = this.authservice.assignHeaders();

        return this.http.get(this.serviceBase + '/api/files/download/' + fileId, { headers: headers })
            .toPromise()
            .catch(this.authservice.handleError);
    }

    _deleteFile(fileToDelete): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/files/delete/', fileToDelete, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }
}
