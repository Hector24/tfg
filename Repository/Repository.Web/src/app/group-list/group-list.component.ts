import { Component, OnInit } from '@angular/core';

import { GroupData } from './group-data';
import { GroupService } from '../group.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent implements OnInit {

    message: string = "";
    groupToCreate = new GroupData();
    groupData: Array<GroupData> = [];
    groupToDelete: string = "";
    authentication = this.authservice._authentication;

    constructor(
        private groupservice: GroupService,
        private authservice: AuthService
    ) { }

    ngOnInit() {
        if (this.authentication.role != 'Admin') {
            this.getUserGroupList();
        } else {
            this.getGroupList();
        }
    }

    createGroup() {
        this.groupservice._createGroup(this.groupToCreate)
            .then(response => {
                this.message = "Group created correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to create group";
            });
    }

    getUserGroupList() { //Se obtiene la lista de grupos a los que pertenece el usuario
        this.groupservice._getUserGroupList()
            .then(response => {
                for (var key in response) {
                    var groupDataAux = new GroupData();
                    groupDataAux.GroupName = response[key].groupName;
                    groupDataAux.Owner = response[key].owner;
                    this.groupData.push(groupDataAux);
                }
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to get group list";
            });
    }

    getGroupList() { //Se obtiene la lista de grupos completa al invocarse por el administrador
        this.groupservice._getGroupList()
            .then(response => {
                for (var key in response) {
                    var groupDataAux = new GroupData();
                    groupDataAux.GroupName = response[key].groupName;
                    groupDataAux.Owner = response[key].owner;
                    this.groupData.push(groupDataAux);
                }
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to get group list";
            });
    }

    deleteGroup(groupFromTable) {
        this.groupToDelete = groupFromTable;
        this.groupservice._deleteGroup(this.groupToDelete)
            .then(response => {
                this.message = "Group deleted correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to delete group";
            });
    }

    startTimer() {
        var timer = setTimeout(() => {
            location.reload();
        }, 2000);
    }
}
