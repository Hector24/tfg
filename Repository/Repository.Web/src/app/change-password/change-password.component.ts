import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PasswordChanges } from './password-changes';
import { AuthService } from '../auth.service';
import { IndexComponent } from '../index/index.component';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

    passwordToChange = new PasswordChanges();
    message: string = "";

    constructor(
        private authservice: AuthService,
        private router: Router,
        private index: IndexComponent
    ) { }

    ngOnInit() {
    }

    changePassword() {
        this.authservice._changePassword(this.passwordToChange, this.authservice._authentication.userName)
            .then(response => {
                this.message = "Your password has changed, you will need lo log in with you new password";
                var timer = setTimeout(() => {
                    this.index.logOut()
                }, 2000);
            },
            response => {
                this.message = "Failed to change password to user due to: Old Password is incorrect or the new password and confirmation password do not match";
            });
    }

    startTimer() {
        var timer = setTimeout(() => {
            this.router.navigate(['home'])
        }, 2000);
    }
}
