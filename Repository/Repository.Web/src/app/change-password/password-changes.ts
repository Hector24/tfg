export class PasswordChanges {

    public OldPassword: string;
    public NewPassword: string;
    public ConfirmPassword: string;

    constructor() { }
}
