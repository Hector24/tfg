import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserChanges } from './user-changes';
import { AuthService } from '../auth.service';
import { IndexComponent } from '../index/index.component';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

    userChanges = new UserChanges();
    message: string = "";

    constructor(
        private authservice: AuthService,
        private router: Router,
        private index: IndexComponent
    ) { }

    ngOnInit() {
    }

    edit() {
        this.authservice._editUser(this.userChanges)
            .then(response => {
                if (this.userChanges.Email == this.authservice._authentication.userName) {
                    this.message = "User updated correctly, you will be redicted to home page in 2 seconds.";
                    this.startTimer();
                }
                else {
                    this.message = "Your email/username have changed, you will need lo log in with you new username";
                    var timer = setTimeout(() => {
                        this.index.logOut()
                    }, 2000);
                }
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to edit user";
            });
    }

    startTimer() {
        var timer = setTimeout(() => {
            this.router.navigate(['home'])
        }, 2000);
    }

}
