import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';
import { IndexComponent } from '../index/index.component';
import { UserListComponent } from '../user-list/user-list.component';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { GroupListComponent } from '../group-list/group-list.component';
import { GroupMembersListComponent } from '../group-members-list/group-members-list.component';
import { FolderListComponent } from '../folder-list/folder-list.component';

const appRoutes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'signup',
        component: SignupComponent
    },
    {
        path: 'user-list',
        component: UserListComponent
    },
    {
        path: 'edit',
        component: EditUserComponent
    },
    {
        path: 'change-password',
        component: ChangePasswordComponent
    },
    {
        path: 'group-list',
        component: GroupListComponent
    },
    {
        path: 'group-members-list',
        component: GroupMembersListComponent
    },
    {
        path: 'folder-list',
        component: FolderListComponent
    },
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
