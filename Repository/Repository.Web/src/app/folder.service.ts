import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { AuthService } from './auth.service';

import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class FolderService {

    serviceBase: string = 'http://localhost:19434';

    constructor(
        private http: Http,
        private authservice: AuthService
    ) { }

    _createFolder(folderData): Promise<Response> {

        folderData.Owner = this.authservice._authentication.userName;

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/folders/create', folderData, { headers: headers })
            .toPromise()
            .catch(this.authservice.handleError);
    }

    _createChildFolder(folderData): Promise<Response> {

        folderData.Owner = this.authservice._authentication.userName;

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/folders/create/childfolder', folderData, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _getParentFolder(parentId) : Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.get(this.serviceBase + '/api/folders/parent/' + parentId, { headers: headers })
            .toPromise()
            .catch(this.authservice.handleError);
    }

    _renameFolder(folderToRename, folderNewName): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/folders/ChangeFolderData/' + folderToRename, folderNewName, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _deleteFolder(folderToDelete): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/folders/delete/', folderToDelete, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }
}
