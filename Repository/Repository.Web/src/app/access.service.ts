import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { AuthService } from './auth.service';

import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AccessService {

    serviceBase: string = 'http://localhost:19434';

    constructor(
        private http: Http,
        private authservice: AuthService
    ) { }

    _grantAccess(accessData): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/access/create', accessData, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _getAccessContent(accessData): Promise<Response> { //Obtiene la configuracion precargada del acceso de un grupo a una carpeta

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/access/get/access', accessData, { headers: headers })
            .toPromise()
            .catch(this.authservice.handleError);
    }

    _getAccessModes(getAccessModes): Promise<Response> { //Obtiene todos los modos de acceso de una carpeta

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/access/get/accessmodes', getAccessModes, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _getUserContent(): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.get(this.serviceBase + '/api/access/content/' + this.authservice._authentication.userName + '/', { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _getUserChildContent(childcontent): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/access/content/child', childcontent, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _getOwnedContent(): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.get(this.serviceBase + '/api/access/content/owned/' + this.authservice._authentication.userName + '/', { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _deleteAccess(accessData): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/access/delete', accessData, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _modifyAccess(accessData): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/access/modify', accessData, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _modifyAccessModes(accessData): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/access/modify/accessmodes', accessData, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }
}
