import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import { RoleData } from './role-data';
import { UserChanges } from '../edit-user/user-changes';
import { PasswordChanges } from '../change-password/password-changes';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

    message: string = "";
    userToDelete: string = "";
    roleToAssign = new RoleData();
    roleData: Array<RoleData> = [];
    roles = ['Admin', 'User without privileges', 'User with privileges'];
    userChanges = new UserChanges();
    passwordToChange = new PasswordChanges();

    constructor(
        private authservice: AuthService
    ) { }

    ngOnInit() {
        this.getUserList();
    }

    getUserList() {
        this.authservice._getUsersList()
            .then(response => {
                for (var key in response) {
                    var roleDataAux = new RoleData();
                    roleDataAux.UserName = response[key].userName;
                    roleDataAux.Role = response[key].roles[0];
                    this.roleData.push(roleDataAux);
                }
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to get users list";
            });
    }

    deleteUser(userFromTable) {
        this.userToDelete = userFromTable;
        this.authservice._deleteUser(this.userToDelete)
            .then(response => {
                this.message = "User deleted correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to delete user" + JSON.stringify(response);
            });
    }

    assingRole(userNameFromTable, roleFromTable) {
        this.roleToAssign.UserName = userNameFromTable;
        this.roleToAssign.Role = roleFromTable;
        this.authservice._assignRole(this.roleToAssign)
            .then(response => {
                this.message = "Role assigned correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to add role to user";
            });
    }

    editUserAdmin(username) {
        this.authservice._editUserAdmin(this.userChanges, username)
            .then(response => {
                this.message = "User updated correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to edit user";
            });
    }

    changePassword(username) {
        this.authservice._changePassword(this.passwordToChange, username)
            .then(response => {
                this.message = "User's password has changed, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                this.message = "Failed to change password to user due to: Old Password is incorrect or the new password and confirmation password do not match";
            });
    }

    startTimer() {
        var timer = setTimeout(() => {
            location.reload();
        }, 2000);
    }
}