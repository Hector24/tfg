import { Component, OnInit } from '@angular/core';

import { GroupService } from '../group.service';
import { MemberData } from './member-data';
import { GroupData } from '../group-list/group-data';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-group-members-list',
  templateUrl: './group-members-list.component.html',
  styleUrls: ['./group-members-list.component.css']
})
export class GroupMembersListComponent implements OnInit {

    message: string = "";
    addMemberData = new MemberData();
    removeMemberData = new MemberData();
    groupData = new GroupData();
    groupMembers: Array<string> = [];
    hideTable: boolean = true;
    authentication = this.authservice._authentication;

    constructor(
        private groupservice: GroupService,
        private authservice: AuthService
    ) { }

    ngOnInit() {
    }

    getGroupMembersList() { //Primero llama al metodo de obtener el grupo para obtener el Owner y no mostrar boton de borrado y luego llama al metodo para obtener los miembros
        this.groupMembers = [];
        this.groupservice._getGroup(this.groupData.GroupName)
            .then(response => {
                this.groupData.Owner = response.json().owner;
                this.groupservice._getGroupMembersList(this.groupData.GroupName)
                    .then(response => {
                        for (var key in response) {
                            var groupMembersAux: string = "";
                            groupMembersAux = response[key].userName;
                            this.groupMembers.push(groupMembersAux);
                        }
                        this.hideTable = false;
                    },
                    response => {
                        var errors = [];
                        for (var key in response.json().modelState) {
                            for (var i = 0; i < response.json().modelState[key].length; i++) {
                                errors.push(response.json().modelState[key][i]);
                            }
                        }
                        this.message = "Failed to show group members list";
                    });
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to show group members list";
            });
    }

    addToGroup() {
        this.addMemberData.GroupName = this.groupData.GroupName;
        this.groupservice._addToGroup(this.addMemberData)
            .then(response => {
                this.message = "User added to group correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to add member to group";
            });
    }

    deleteFromGroup(memberFromTable) {
        this.removeMemberData.UserName = memberFromTable;
        this.removeMemberData.GroupName = this.groupData.GroupName;
        this.groupservice._deleteFromGroup(this.removeMemberData)
            .then(response => {
                this.message = "User deleted from group correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to delete member from group";
            });
    }

    startTimer() {
        var timer = setTimeout(() => {
            location.reload();
        }, 2000);
    }
}
