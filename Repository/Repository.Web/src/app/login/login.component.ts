import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginData } from './login-data';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginData = new LoginData();
    message: string = "Please, insert your user name and password to log in";

    constructor(
        private authservice: AuthService,
        private router: Router,
    ) { }

    ngOnInit() {
    }

    login() {
        this.authservice._login(this.loginData)
            .then(response => {
                this.router.navigate(['home']);
            },
            response => {
                this.message = "Failed to login user";
            });
    }
}
