import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { RouterModule, Routes, Router } from '@angular/router';

import { LocalStorageModule, LocalStorageService } from 'angular-2-local-storage';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';


import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { AuthService } from './auth.service';
import { IndexComponent } from './index/index.component';
import { UserListComponent } from './user-list/user-list.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { GroupService } from './group.service';
import { GroupListComponent } from './group-list/group-list.component';
import { GroupMembersListComponent } from './group-members-list/group-members-list.component';
import { FolderService } from './folder.service';
import { AccessService } from './access.service'
import { FolderListComponent } from './folder-list/folder-list.component';
import { FileService } from './file.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    IndexComponent,
    UserListComponent,
    EditUserComponent,
    ChangePasswordComponent,
    GroupListComponent,
    GroupMembersListComponent,
    FolderListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2Bs3ModalModule,
    LocalStorageModule.withConfig({
        prefix: 'app-root',
        storageType: 'localStorage'
        //storageType: 'sessionStorage'
    }),
    AppRoutingModule
  ],
  providers: [
      AuthService,
      GroupService,
      FolderService,
      AccessService,
      FileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
    constructor(private authservice: AuthService) { //Si se quisiese cambiar que no se mantuviese logeado para evitar la expiracion del token habria que borrar o comentar esto
        this.authservice._fillAuthData();
    }
}
