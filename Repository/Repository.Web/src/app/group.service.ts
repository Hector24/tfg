import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { AuthService } from './auth.service';

import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class GroupService {

    serviceBase: string = 'http://localhost:19434';

    constructor(
        private http: Http,
        private authservice: AuthService
    ) { }

    _createGroup(groupData): Promise<Response> {

        groupData.Owner = this.authservice._authentication.userName;

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/groups/create', groupData, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _getGroup(groupName): Promise<Response> { //No pasa response.json() as Response porque si no hay problemas a la hora de obtener los datos del cuerpo

        var headers = this.authservice.assignHeaders();

        return this.http.get(this.serviceBase + '/api/groups/group/' + groupName, { headers: headers })
            .toPromise()
            .catch(this.authservice.handleError);
    }

    _getGroupList(): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.get(this.serviceBase + '/api/groups/list', { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _getUserGroupList(): Promise<Response> {

        var headers = this.authservice.assignHeaders();
        
        return this.http.get(this.serviceBase + '/api/groups/user/list/' + this.authservice._authentication.userName + '/', { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _deleteGroup(groupToDelete): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.delete(this.serviceBase + '/api/groups/delete/' + groupToDelete, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _getGroupMembersList(groupName): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.get(this.serviceBase + '/api/groups/list/members/' + groupName, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _addToGroup(addMemberData): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.post(this.serviceBase + '/api/groups/AddToGroup', addMemberData, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }

    _deleteFromGroup(removeMemberData): Promise<Response> {

        var headers = this.authservice.assignHeaders();

        return this.http.delete(this.serviceBase + '/api/groups/RemoveFromGroup', { headers: headers, body: removeMemberData })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.authservice.handleError);
    }
}