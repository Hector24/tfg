import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { LocalStorageService } from 'angular-2-local-storage';
import { tokenNotExpired, JwtHelper } from 'angular2-jwt/angular2-jwt';


import { authentication } from './authentication';

import { Observable } from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService {

    serviceBase: string = 'http://localhost:19434';

    _authentication = new authentication();

    jwtHelper: JwtHelper = new JwtHelper();

    constructor(
        private http: Http,
        private localStorage: LocalStorageService
    ) { }

    _logOut() {

        this.localStorage.remove('authorizationData');

        this._authentication.isAuth = false;
        this._authentication.userName = "";
        this._authentication.role = "";
    }

    _saveRegistration(registration): Promise<Response> {

        var headers = this.assignHeaders();

        this._logOut();

        return this.http.post(this.serviceBase + '/api/accounts/create', registration, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.handleError);
    }

    _login(loginData): Promise<Response> {

        var headers = this.assignHeaders();

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.Password;

        return this.http.post(this.serviceBase + '/oauth/token', data, { headers: headers })
            .toPromise()
            .then(response => {
                this.localStorage.set('authorizationData', JSON.stringify({ token: response.json().access_token, userName: loginData.userName }));

                this._authentication.isAuth = true;
                this._authentication.userName = loginData.userName;

                var authData = JSON.parse(this.localStorage.get<any>('authorizationData'));
                var roles = this.jwtHelper.decodeToken(authData.token);
                this._authentication.role = roles.role;


                return Promise.resolve(response);

            },
            response => {
                this._logOut();
                return Promise.reject(response);
            });
    }

    _fillAuthData() {
        var authData = JSON.parse(this.localStorage.get<any>('authorizationData')); //Esto es lo que devuelve localStorage {"token":"el valor del token","userName":"api3@api.com"}
        if (authData) {
            this._authentication.isAuth = true;
            this._authentication.userName = authData.userName;
            var roles = this.jwtHelper.decodeToken(authData.token);
            this._authentication.role = roles.role;
        }
    }

    _assignRole(roleData): Promise<Response> {

        var headers = this.assignHeaders();

        return this.http.post(this.serviceBase + '/api/accounts/ChangeUserType', roleData, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.handleError);
    }

    handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }

    _getUsersList(): Promise<Response> {

        var headers = this.assignHeaders();

        return this.http.get(this.serviceBase + '/api/accounts/users', { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.handleError);
    }

    _deleteUser(userToDelete): Promise<Response> {

        var headers = this.assignHeaders();

        return this.http.delete(this.serviceBase + '/api/accounts/user/' + userToDelete + '/', { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.handleError);
    }

    _editUser(userChanges): Promise<Response> { //No pasa response.json() as Response porque si no hay problemas a la hora de obtener los datos del cuerpo

        var headers = this.assignHeaders();

        return this.http.post(this.serviceBase + '/api/accounts/ChangeUserData/' + this._authentication.userName + '/', userChanges, { headers: headers })
            .toPromise()
            .catch(this.handleError);
    }

    _editUserAdmin(userChanges, username): Promise<Response> { //Esta funcion la usa el administrador para cambiar los datos de un usuario
        //usa un email distinto al que hay en this._authentication.userName

        var headers = this.assignHeaders();

        return this.http.post(this.serviceBase + '/api/accounts/ChangeUserData/' + username + '/', userChanges, { headers: headers })
            .toPromise()
            .catch(this.handleError);
    }

    _changePassword(passwordToChange, username): Promise<Response> {

        var headers = this.assignHeaders();

        return this.http.post(this.serviceBase + '/api/accounts/changepassword/' + username + '/', passwordToChange, { headers: headers })
            .toPromise()
            .then(response => response.json() as Response)
            .catch(this.handleError);
    }

    assignHeaders(): Headers {
        if (this._authentication.isAuth) {
            var authData = JSON.parse(this.localStorage.get<any>('authorizationData'));
            var headers = new Headers({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + authData.token });
            return headers;
        }
        else {
            var headers = new Headers({ 'Accept': 'application/json', 'Content-Type': 'application/json' });
        }
    }
}