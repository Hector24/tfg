import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { Registration } from './registration';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

    savedSuccessfully: boolean = false;
    message: string = "";
    registration = new Registration();

    constructor(
        private authservice: AuthService,
        private router: Router
    ) { }

    ngOnInit() {
    }

    signUp() {
        this.authservice._saveRegistration(this.registration)
            .then(response => { //response => equivale al funcion(response)
                this.savedSuccessfully = true;
                this.message = "User has been registered successfully, you will be redicted to login page in 2 seconds.";
                this.startTimer();
            },
            response => { 
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to register user";
            });
    }

    startTimer() {
        var timer = setTimeout(() => {
            this.router.navigate(['login'])
        }, 2000);
    }
}
