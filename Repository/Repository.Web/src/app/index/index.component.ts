import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    authentication = this.authservice._authentication;

    constructor(
        private authservice: AuthService,
        private router: Router
    ) { }

    logOut() {
        this.authservice._logOut();
        this.router.navigate(['home']);
    }

    ngOnInit() {
    }
}
