export class AccessData {

    public GroupName: string;
    public FolderId: string;
    public AccessPermission: string;
    public AccessMode: Array<string> = [];

    constructor() { }
}
