export class FileData {

    public id: string;
    public ParentId: string;
    public FileName: string;
    public Owner: string;
    public Format: string;
    public AccessModes = [
        { name: 'Read_only', checked: false },
        { name: 'Read_Write', checked: false },
        { name: 'Download', checked: false },
        { name: 'Create', checked: false },
        { name: 'Delete', checked: false }
    ];

    constructor() { }
}
