export class FolderData {

    public id: string;
    public ParentId: string;
    public FolderName: string;
    public Owner: string;
    public Files: Array<string>;
    public AccessModes = [
        { name: 'Read_only', checked: false},
        { name: 'Read_Write', checked: false},
        { name: 'Download', checked: false},
        { name: 'Create', checked: false},
        { name: 'Delete', checked: false}
    ];
    //public Accessible: boolean = false; //Flag para deshabilitar el boton Expand a las carpetas que se indexan solo por propiedad
    constructor() { }

}