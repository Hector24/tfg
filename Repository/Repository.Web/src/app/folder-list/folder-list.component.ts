import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FolderData } from './folder-data';
import { FolderService } from '../folder.service';
import { AuthService } from '../auth.service';
import { AccessService } from '../access.service';
import { AccessData } from './access-data';
import { ChildData } from './child-data';
import { FileData } from './file-data';
import { FileService } from '../file.service';

@Component({
  selector: 'app-folder-list',
  templateUrl: './folder-list.component.html',
  styleUrls: ['./folder-list.component.css']
})
export class FolderListComponent implements OnInit {

    message: string = "";

    //Variables carpetas
    route: string = "Root";
    folderData = new FolderData();
    folderList: Array<FolderData> = []; //Este es el array que se muestra en el HTML
    folderListAux: Array<FolderData> = []; //Aqui se guardan todas las carpetas para luego ir seleccionandolas y guardandolas en el array que se va a mostrar
    folderNewName = new FolderData();
    folderToRename: string = "";
    folderToDelete = new FolderData();
    root_parentId: string = "0AMzZTPFHBE9AUk9PVA";
    childData = new ChildData(); //Se usa para obtener de la API el contenido accesible dentro de una carpeta determinada

    authentication = this.authservice._authentication;

    //Variables datos de acceso
    accessData = new AccessData();
    AccessModes = [
        { name: 'Read_only', checked: false, disabled: false },
        { name: 'Read_Write', checked: false, disabled: false },
        { name: 'Download', checked: false, disabled: false },
        { name: 'Create', checked: false, disabled: false },
        { name: 'Delete', checked: false, disabled: false }
    ];
    AccessPermission = ['Access with inheritance', 'Access without inheritance', 'Access denied'];

    //Variables para obtener modos de acceso
    modes: Array<string> = []; //Se guardan los modos de acceso obtenidos para los contenidos del usuario
    getAccessModesData = new ChildData(); //Se utiliza para enviar a la API los datos del usuario y de la carpeta de la que hay que obtener los modos de acceso

    //Variables para la subida de ficheros
    fileData = new FileData(); //Se usa apra enviar a la API la carpeta en la que tiene que subir el fichero
    formData: FormData = new FormData(); //Se utiliza para que el evento que detecta que se ha seleccionado un fichero del PC env�e dicho fichero en el POST
    filesList: Array<FileData> = [];

    constructor(
        private router: Router,
        private folderservice: FolderService,
        private authservice: AuthService,
        private accessservice: AccessService,
        private fileservice: FileService,
    ) { }

    ngOnInit() {
        this.getUserContentList();
    }

    getUserContentList() { //Se obtiene la lista de carpetas a las que tiene acceso directo el usuario
        this.folderList = [];
        this.accessservice._getUserContent()
            .then(response => {
                for (var key in response) {
                    var folderDataAux = new FolderData();
                    folderDataAux.id = response[key].id;
                    folderDataAux.FolderName = response[key].folderName;
                    folderDataAux.Owner = response[key].owner;
                    folderDataAux.ParentId = response[key].parentId;
                    folderDataAux.Files = response[key].files;
                    //folderDataAux.Accessible = true; //Habilita la expansi�n por tener un acceso establecido a dicha carpeta
                    this.folderList.push(folderDataAux);
                }
                this.getAccessModes(); //Llama a la funci�n para obtener los modos de acceso de cada carpeta obtenida
                this.accessservice._getOwnedContent() //A�adido para indexar las carpetas propietarias sin acceso establecido
                    .then(response => {
                        this.folderListAux = [];
                        for (var key in response) {
                            let folderDataAux = new FolderData();
                            let contained: boolean = false; //Flag para evitar duplicaciones de carpetas de las que el usuario es propietario 
                            folderDataAux.id = response[key].id;
                            folderDataAux.FolderName = response[key].folderName;
                            folderDataAux.Owner = response[key].owner;
                            folderDataAux.ParentId = response[key].parentId;
                            folderDataAux.Files = response[key].files;
                            for (var x in this.folderList) { //Recorremos el array con las carpetas ya indexadas
                                if (this.folderList[x].FolderName == folderDataAux.FolderName) { //Si alguna coincide con las obtenidas para el caso de que sea propietario
                                    contained = true; //Se activa el flag de contenido para no a�adirlo
                                }
                            }

                            if (!contained) { //Si el flag esta desactivado se a�ade
                                this.folderListAux.push(folderDataAux);
                            }
                        }

                        this.folderList = this.folderList.concat(this.folderListAux); //Se concatenan los 2 arrays para convertirlo en 1 solo
                        
                    },
                    response => {
                        var errors = [];
                        for (var key in response.json().modelState) {
                            for (var i = 0; i < response.json().modelState[key].length; i++) {
                                errors.push(response.json().modelState[key][i]);
                            }
                        }
                        this.message = "Failed to get user's owned content";
                    });
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to get user's content list";
            });
    }

    getAccessModes() { //Obtiene todos los modos de acceso a esa carpeta y los a�ade a un array que posteriormente se a�adir� a la clase de la carpeta
        for (var folder in this.folderList) {
            let folderAux = new FolderData(); //Variable auxiliar LET que evita el fallo de que el bucle use solo el ultimo valor para el POST
            folderAux = this.folderList[folder];
            this.getAccessModesData.FolderId = this.folderList[folder].id;
            this.getAccessModesData.UserName = this.authentication.userName;
            this.accessservice._getAccessModes(this.getAccessModesData)
                .then(response => {
                    this.modes = [];
                    for (var key in response) {
                        for (var key2 in response[key].accessMode) {
                            this.modes.push(response[key].accessMode[key2]);
                        }
                    }
                    
                    for (var x in this.modes) {
                        for (var y in folderAux.AccessModes) {
                            if (this.modes[x] == folderAux.AccessModes[y].name) {
                                folderAux.AccessModes[y].checked = true;
                            }
                        }
                    }
                },
                response => {
                    var errors = [];
                    for (var key in response.json().modelState) {
                        for (var i = 0; i < response.json().modelState[key].length; i++) {
                            errors.push(response.json().modelState[key][i]);
                        }
                    }
                    this.message = "Failed to get folders' access modes list";
                });     
            this.folderList[folder] = folderAux; //Asigna los nuevos valores al array original
        }
    }

    getFilesAccessModes(parentId) {
        this.getAccessModesData.FolderId = parentId; //Usa el Id de la carpeta de la que se obtuvieron los ficheros para obtener sus modos de acceso
        this.getAccessModesData.UserName = this.authentication.userName;
        this.accessservice._getAccessModes(this.getAccessModesData)
            .then(response => {
                this.modes = [];
                for (var key in response) {
                    for (var key2 in response[key].accessMode) {
                        this.modes.push(response[key].accessMode[key2]);
                    }
                }

                for (var file in this.filesList) { //Se asignan los modos de acceso obtenidos de la carpeta a todos los ficheros
                    let fileAux = new FileData(); //Variable auxiliar LET que evita el fallo de que el bucle use solo el ultimo valor para el POST
                    fileAux = this.filesList[file];
                    for (var x in this.modes) {
                        for (var y in fileAux.AccessModes) {
                            if (this.modes[x] == fileAux.AccessModes[y].name) {
                                fileAux.AccessModes[y].checked = true;
                            }
                        }
                    }
                    this.filesList[file] = fileAux; //Asigna los nuevos valores al array original
                }
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "This folder doesn't have an access established, please grant access first.";
            });
    }

    getUserChildContent(folder) { //Se obtiene la lista de carpetas contenidas en una carpeta determinada y se muestran solo a las que tiene acceso
        this.route = this.route + " > " + folder.FolderName;
        this.folderList = [];
        this.filesList = [];
        this.childData.FolderId = folder.id;
        this.childData.UserName = this.authentication.userName;
        this.accessservice._getUserChildContent(this.childData)
            .then(response => {
                for (var key in response) {
                    var folderDataAux = new FolderData();
                    folderDataAux.id = response[key].id;
                    folderDataAux.FolderName = response[key].folderName;
                    folderDataAux.Owner = response[key].owner;
                    folderDataAux.ParentId = response[key].parentId;
                    folderDataAux.Files = response[key].files;
                    //folderDataAux.Accessible = true; //Habilita la expansi�n por tener un acceso establecido a dicha carpeta
                    this.folderList.push(folderDataAux);
                }
                this.getAccessModes(); //Llama a la funci�n para obtener los modos de acceso de cada carpeta obtenida
                this.fileservice._getFiles(this.childData.FolderId) //Llama a la rutina para obtener la informacion de los archivos que hay en la carpeta, si los hubiese
                    .then(response => {
                        for (var key in response) {
                            var fileDataAux = new FileData();
                            fileDataAux.id = response[key].id;
                            fileDataAux.FileName = response[key].fileName;
                            fileDataAux.Owner = response[key].owner;
                            fileDataAux.ParentId = response[key].parentId;
                            fileDataAux.Format = response[key].format;
                            this.filesList.push(fileDataAux);
                        }
                        this.getFilesAccessModes(this.childData.FolderId);
                    },
                    response => {
                        var errors = [];
                        for (var key in response.json().modelState) {
                            for (var i = 0; i < response.json().modelState[key].length; i++) {
                                errors.push(response.json().modelState[key][i]);
                            }
                        }
                        this.message = "Failed to get files list";
                    });

            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "You don't have access to this point of the hierarchy";
            });
    }

    collapseFolder(folder) { //Falla al volver a Root
        this.folderList = [];
        this.filesList = [];
        this.folderservice._getParentFolder(folder.ParentId) //Obtiene la carpeta que se quiere colapsar
            .then(response => {
                this.route = this.route.replace(" > " + response.json().folderName, "");
                if (this.route == "Root") {
                    this.refreshPage();
                } else {
                    this.folderservice._getParentFolder(response.json().parentId) //Obtiene el padre de la carpeta que se va a colapsar
                        .then(response => {
                            this.route = this.route.replace(" > " + response.json().folderName, "");
                            var folderDataAux = new FolderData(); //Variable auxiliar para pasarle el objeto carpeta a la funcion de obtener el contenido de una carpeta en concreto
                            folderDataAux.id = response.json().id;
                            folderDataAux.FolderName = response.json().folderName;
                            folderDataAux.Owner = response.json().owner;
                            folderDataAux.ParentId = response.json().parentId;
                            folderDataAux.Files = response.json().files;
                            this.getUserChildContent(folderDataAux); //Obtiene el contenido del padre de la carpeta que se tiene que colapsar para subir un nivel en la jerarquia
                        },
                        response => {
                            var errors = [];
                            for (var key in response.json().modelState) {
                                for (var i = 0; i < response.json().modelState[key].length; i++) {
                                    errors.push(response.json().modelState[key][i]);
                                }
                            }
                            this.message = "Failed to get child folder list";
                        });
                }
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to get child folder list";
            });
    }
    createFolder() {
        this.folderservice._createFolder(this.folderData)
            .then(response => {
                this.message = "Folder created correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to create folder";
            });
    }

    createChildFolder(parentId) { //Rutina para la creacion de carpetas hijo
        this.folderData.ParentId = parentId;
        this.folderservice._createChildFolder(this.folderData)
            .then(response => {
                this.message = "Folder created correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to create folder";
            });
    }

    renameFolder(ParentId, folderToRename) { //Para renombrar las carpetas desde el boton de la vista
        this.folderNewName.ParentId = ParentId;
        this.folderservice._renameFolder(folderToRename, this.folderNewName)
            .then(response => {
                this.message = "Folder renamed correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to rename folder";
            });
    }

    deleteFolder(folderFromList, parentIdFromList) {
        this.folderToDelete.FolderName = folderFromList;
        this.folderToDelete.ParentId = parentIdFromList;
        this.folderservice._deleteFolder(this.folderToDelete)
            .then(response => {
                this.message = "Folder deleted correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to delete folder";
            });
    }

    grantAccess(folderIdFromList) { //Crea una relacion entre un grupo y la carpeta con los permisos y modos de acceso seleccionados
        this.accessData.AccessMode = []; //Limpia los modos de acceso a a�adir para a�adir los nuevos
        for (var x in this.AccessModes) {
            if (this.AccessModes[x].checked) {
                this.accessData.AccessMode.push(this.AccessModes[x].name);
            }
        }
        //this.message = JSON.stringify(this.accessData.AccessMode); //Eliminar
        this.accessData.FolderId = folderIdFromList;
        this.accessservice._grantAccess(this.accessData)
            .then(response => {
                this.message = "Access granted correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to grant access";
            });
    }

    getAccessContent(folderIdFromList) { //Para precargar la configuracion de acceso de un determinado grupo
        for (var mode in this.AccessModes) { //Limpia los checkbox para escribir la configuracion que tiene actualmete
            this.AccessModes[mode].checked = false;
        }
        this.accessData.FolderId = folderIdFromList;
        this.accessservice._getAccessContent(this.accessData)
            .then(response => {
                this.accessData.AccessPermission = response.json().accessPermission;
                this.accessData.AccessMode = response.json().accessMode;
                for (var mode in this.accessData.AccessMode) {
                    for (var selected in this.AccessModes) {
                        if (this.accessData.AccessMode[mode] == this.AccessModes[selected].name) {
                            this.AccessModes[selected].checked = true;
                        }
                    }
                }
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "This group doesn't have access to this folder determined yet, please fill in the formulary to establish it.";
            });
    }

    updateCheckedOptions(option, event) { //Esta funcion a�ade los checkbox marcados a los modos de acceso a a�adir
        if (event.target.checked) { //Si se selecciona el checkbox lo a�ade 
            for (var mode in this.AccessModes) {
                if (this.AccessModes[mode].name == option) {
                    this.AccessModes[mode].checked = true;
                    if (option == "Read_Write") { //Si se activa esto desactiva y desmarca solo Read_only
                        this.AccessModes[0].checked = false;
                        this.AccessModes[0].disabled = true; //El [0] es el puesto de Read_only
                    }
                    if (option == "Read_only") { //Si se activa esto se desactiva y desmarcan R/W, Download y Creation
                        this.AccessModes[1].checked = false;
                        this.AccessModes[3].checked = false;
                        this.AccessModes[4].checked = false;
                        this.AccessModes[1].disabled = true;
                        this.AccessModes[3].disabled = true;
                        this.AccessModes[4].disabled = true;
                    }
                    //this.message = JSON.stringify(this.AccessModes); //Eiminar
                }
            }
        } else {
            for (var mode in this.AccessModes) { //Si se desmarca la casilla lo borra 
                if (this.AccessModes[mode].name == option) {
                    this.AccessModes[mode].checked = false;
                    if (option == "Read_Write") { //Si se desactiva esto activa solo Read_only
                        this.AccessModes[0].disabled = false; //El [0] es el puesto de Read_only
                    }
                    if (option == "Read_only") { //Si se desactiva esto activa R/W, Download y Creation
                        this.AccessModes[1].disabled = false;
                        this.AccessModes[3].disabled = false;
                        this.AccessModes[4].disabled = false;
                    }
                    //this.message = JSON.stringify(this.AccessModes); //Eliminar
                }
            }
        }
    }

    modifyAccess(folderIdFromList) { //Envia los nuevos permisos y accesos 
        this.accessData.AccessMode = []; //Limpia los modos de acceso a a�adir para a�adir los nuevos
        for (var x in this.AccessModes) {
            if (this.AccessModes[x].checked) {
                this.accessData.AccessMode.push(this.AccessModes[x].name);
            }
        }
        //this.message = JSON.stringify(this.accessData.AccessMode); //Eliminar
        this.accessData.FolderId = folderIdFromList;
        this.accessservice._modifyAccess(this.accessData)
            .then(response => {
                this.message = "Access modified correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to modify access";
            });
    }

    deleteAccess(folderIdFromList) { //Elimina la relaccion de acceso del grupo hacia la carpeta en cuestion
        this.accessData.FolderId = folderIdFromList;
        this.accessservice._deleteAccess(this.accessData)
            .then(response => {
                this.message = "Access deleted correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to delete access";
            });
    }

    fileChange(event) { //Evento que detecta un cambio en el seleccionador de archivos del HTML
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            this.formData.append('uploadFile', file, file.name);
            this.fileData.FileName = file.name;
        }
    }

    uploadFile(folder) { //Rutina para subir fichero
        this.fileData.Owner = folder.Owner;
        this.fileData.ParentId = folder.id;
        this.fileservice._uploadFiledB(this.fileData) //Primero lo registra en la DB
            .then(response => {
                this.fileservice._uploadFileDrive(this.formData, this.fileData.ParentId) //Luego lo sube a GDrive porque los 2 a la vez hab�a conflicto al hacerlo
                    .then(response => {
                        this.message = "File uploaded correctly, page will be refreshed in 2 seconds.";
                        this.startTimer();
                    },
                    response => {
                        var errors = [];
                        for (var key in response.json().modelState) {
                            for (var i = 0; i < response.json().modelState[key].length; i++) {
                                errors.push(response.json().modelState[key][i]);
                            }
                        }
                        this.message = "Failed to upload file to Google Drive";
                    });
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to upload file to database";
            });
    }

    downloadFile(fileId) { //Rutina para obtener y redireccionar al navegador para descargar el fichero en cuestion
        this.fileservice._downloadFile(fileId)
            .then(response => {
                window.location.href = response.json().webContentLink;
            },
            response => {
                
            });
    }

    updateFile(fileParentId) { //Rutina que actualiza el archivo en GDrive
        this.fileservice._updateFile(this.formData, fileParentId)
            .then(response => {
                this.message = "File updated correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to update file of GDrive";
            });
    }

    deleteFile(fileToDelete) { //Pasa los datos del fichero para eliminarlo
        this.fileservice._deleteFile(fileToDelete)
            .then(response => {
                this.message = "File deleted correctly, page will be refreshed in 2 seconds.";
                this.startTimer();
            },
            response => {
                var errors = [];
                for (var key in response.json().modelState) {
                    for (var i = 0; i < response.json().modelState[key].length; i++) {
                        errors.push(response.json().modelState[key][i]);
                    }
                }
                this.message = "Failed to delete file";
            });
    }

    refreshPage() {
        location.reload();
    }

    startTimer() {
        var timer = setTimeout(() => {
            location.reload();
        }, 2000);
    }
}
