import { RepositoryWebPage } from './app.po';
describe('repository-web App', function () {
    var page;
    beforeEach(function () {
        page = new RepositoryWebPage();
    });
    it('should display message saying app works', function () {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('app works!');
    });
});
//# sourceMappingURL=C:/Users/Hector/Dropbox/Apuntes/Proyecto/Codigo/TFG/Repository/Repository.Web/e2e/app.e2e-spec.js.map