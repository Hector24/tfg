import { browser, element, by } from 'protractor';
var RepositoryWebPage = (function () {
    function RepositoryWebPage() {
    }
    RepositoryWebPage.prototype.navigateTo = function () {
        return browser.get('/');
    };
    RepositoryWebPage.prototype.getParagraphText = function () {
        return element(by.css('app-root h1')).getText();
    };
    return RepositoryWebPage;
}());
export { RepositoryWebPage };
//# sourceMappingURL=C:/Users/Hector/Dropbox/Apuntes/Proyecto/Codigo/TFG/Repository/Repository.Web/e2e/app.po.js.map