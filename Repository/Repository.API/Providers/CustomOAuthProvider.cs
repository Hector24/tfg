﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Repository.API.Infrastructure;
using Repository.API.Models;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Repository.API.Providers
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
 
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        { //Validated esta vacio porque consideramos que el cliente en Angular2 es un cliente en el que confiamos
            context.Validated();
            return Task.FromResult<object>(null);
        }
 
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        { //Recibe el nombre de usuario y password y se encarga de obtener si ese usario con esas credenciales existe o no
          //si existe se crea un "ticket" que se pasa al proceso de autenticacion OAuth 2.0

            var allowedOrigin = "*";
 
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });
 
            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
 
            User user = await userManager.FindAsync(context.UserName, context.Password);
 
            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }
 
            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, "JWT");
        
            var ticket = new AuthenticationTicket(oAuthIdentity, null);
            
            context.Validated(ticket);
           
        }
    }
}