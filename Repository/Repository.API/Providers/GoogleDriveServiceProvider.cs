﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using System.Threading;

namespace Repository.API.Providers
{
    public class GoogleDriveServiceProvider
    {
        public DriveService GetCredential()
        { //Le pasa al Broker de GDrive las credenciales obtenidas de la API de Google y el scope deseado
          //para que Google nos devuelva una instancia del servicio de GDrive y asi poder interaccionar con dicho servicio

            UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                new ClientSecrets
                {
                    ClientId = "896081485538-rls5vdr0upgjuudps0n8r1a7qkvg7tbt.apps.googleusercontent.com",
                    ClientSecret = "IEPuUT6E0NFiKF6q39Jj9lg1",
                },
                new[] { DriveService.Scope.Drive },
                "user",
                CancellationToken.None).Result;
            // Create the service.
            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Repository",
            });
            return service;
        }
    }
}