﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Repository.API.Models
{
    public class Access
    {
        [Display(Name = "Access Permission")]
        public string AccessPermission { get; set; }

        [Display(Name = "Access Mode")]
        public List<string> AccessMode { get; set; }
    }
}