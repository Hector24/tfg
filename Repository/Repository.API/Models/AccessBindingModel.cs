﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Repository.API.Models
{
    public class AccessBindingModel
    {
        [Required]
        [Display(Name = "Access Permission")]
        public string AccessPermission { get; set; }

        [Required]
        [Display(Name = "Access Mode")]
        public List<string> AccessMode { get; set; }

        [Required]
        [Display(Name = "Group Name")]
        public string GroupName { get; set; }

        [Required]
        [Display(Name = "Folder Id")]
        public string FolderId { get; set; }
    }

    public class GetAccessBindingModel
    {
        [Display(Name = "Access Permission")]
        public string AccessPermission { get; set; }

        [Display(Name = "Access Mode")]
        public List<string> AccessMode { get; set; }

        [Required]
        [Display(Name = "Group Name")]
        public string GroupName { get; set; }

        [Required]
        [Display(Name = "Folder Id")]
        public string FolderId { get; set; }
    }

    public class ChildContentBindingModel
    {

        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Folder Id")]
        public string FolderId { get; set; }
    }
}