﻿using System.ComponentModel.DataAnnotations;

namespace Repository.API.Models
{
    public class CreateGroupBindingModel
    {
        [Required]
        [Display(Name = "Group Name")]
        public string GroupName { get; set; }

        [Required]
        [Display(Name = "Group Owner")]
        public string Owner { get; set; }
    }

    public class AddToGroupBindingModel
    {
        [Required]
        [Display(Name = "Group Name")]
        public string GroupName { get; set; }

        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }
    }
}