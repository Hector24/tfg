﻿using Repository.API.Infrastructure;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http.Routing;

namespace Repository.API.Models
{
    public class ModelFactory
    {
        private UrlHelper _UrlHelper;
        private ApplicationUserManager _AppUserManager;

        public ModelFactory(HttpRequestMessage request, ApplicationUserManager appUserManager)
        {
            _UrlHelper = new UrlHelper(request);
            _AppUserManager = appUserManager;
        }

        public UserReturnModel Create(User appUser) //Estos son los modelos que se usaran en las respuestas para enviar los datos de las consultas a la DB
        {
            return new UserReturnModel
            {
                Url = _UrlHelper.Link("GetUserById", new { id = appUser.Id }),
                UserName = appUser.Email,
                FirstName = appUser.FirstName,
                LastName = appUser.LastName,
                Email = appUser.Email,
                Roles = appUser.Roles,
            };
        }

        public GroupReturnModel CreateGroup(Group appGroup)
        {
            return new GroupReturnModel
            {
                Url = _UrlHelper.Link("GetGroupByName", new { groupname = appGroup.GroupName }),
                GroupName = appGroup.GroupName,
                Owner = appGroup.Owner,
            };
        }

        public FolderReturnModel CreateFolder (Folder appFolder)
        {
            return new FolderReturnModel
            {
                Url = _UrlHelper.Link("GetFolderByName", new { foldername = appFolder.FolderName }),
                id = appFolder.id,
                ParentId = appFolder.ParentId,
                FolderName = appFolder.FolderName,
                Owner = appFolder.Owner,
                Files = appFolder.Files,
                CreationDate = appFolder.CreationDate,
                ModificationDate = appFolder.ModificationDate,
            };
        }

        public AccessReturnModel CreateAccess(Access appAccess)
        {
            return new AccessReturnModel
            {
                AccessPermission = appAccess.AccessPermission,
                AccessMode = appAccess.AccessMode,
            };
        }

        public FileReturnModel CreateFile(File appFile)
        {
            return new FileReturnModel
            {
                Url = _UrlHelper.Link("GetFileByName", new { filename = appFile.FileName }),
                id = appFile.id,
                ParentId = appFile.ParentId,
                FileName = appFile.FileName,
                Owner = appFile.Owner,
                Format = appFile.Format,
                CreationDate = appFile.CreationDate,
                ModificationDate = appFile.ModificationDate,
            };
        }
    }

    public class UserReturnModel //Clases que definen los datos que iran en los modelos retornables en las respuestas de la API
    { 
        public string Url { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public IList<string> Roles { get; set; }
    }

    public class GroupReturnModel
    {
        public string Url { get; set; }
        public string GroupName { get; set; }
        public string Owner { get; set; }
    }

    public class FolderReturnModel
    {
        public string Url { get; set; }
        public string id { get; set; }
        public string ParentId { get; set; }
        public string FolderName { get; set; }
        public string Owner { get; set; }
        public List<string> Files { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModificationDate { get; set; }
    }

    public class AccessReturnModel
    {
        public string AccessPermission { get; set; }
        public List<string> AccessMode { get; set; }
    }

    public class FileReturnModel
    {
        public string Url { get; set; }
        public string id { get; set; }
        public string ParentId { get; set; }
        public string FileName { get; set; }
        public string Owner { get; set; }
        public string Format { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModificationDate { get; set; }
    }
}