﻿using System.ComponentModel.DataAnnotations;

namespace Repository.API.Models
{
    public class Group
    {
        [Required]
        [Display(Name = "Group Name")]
        public string GroupName { get; set; }

        [Required]
        [Display(Name = "Group Owner")]
        public string Owner { get; set; }
    }
}