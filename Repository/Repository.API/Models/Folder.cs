﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Repository.API.Models
{
    public class Folder
    {
        [Display(Name = "Id")]
        public string id { get; set; }

        [Display(Name = "Parent Id")]
        public string ParentId { get; set; }

        [Required]
        [Display(Name = "Folder Name")]
        public string FolderName { get; set; }

        [Required]
        [Display(Name = "Folder Owner")]
        public string Owner { get; set; }

        [Display(Name = "Files")]
        public List<string> Files { get; set; }

        [Required]
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }

        [Required]
        [Display(Name = "Modification Date")]
        public DateTime ModificationDate { get; set; }
    }
}