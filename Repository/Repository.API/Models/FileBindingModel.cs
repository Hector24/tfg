﻿using System.ComponentModel.DataAnnotations;

namespace Repository.API.Models
{
    public class CreateFileBindingModel
    {
        [Display(Name = "Parent Id")]
        public string ParentId { get; set; }

        [Required]
        [Display(Name = "File Name")]
        public string FileName { get; set; }

        [Required]
        [Display(Name = "File Owner")]
        public string Owner { get; set; }
    }

    public class DeleteFileBindingModel
    {
        [Display(Name = "Parent Id")]
        public string ParentId { get; set; }

        [Required]
        [Display(Name = "File Name")]
        public string FileName { get; set; }
    }

    public class ChangeFileDataBindingModel
    {
        [Display(Name = "Parent Id")]
        public string ParentId { get; set; }

        [Required]
        [Display(Name = "File Name")]
        public string FileName { get; set; }
    }
}