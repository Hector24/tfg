﻿using System.ComponentModel.DataAnnotations;

namespace Repository.API.Models
{
    public class CreateFolderBindingModel
    {
        [Display(Name = "Parent Id")]
        public string ParentId { get; set; }

        [Required]
        [Display(Name = "Folder Name")]
        public string FolderName { get; set; }

        [Required]
        [Display(Name = "Folder Owner")]
        public string Owner { get; set; }
    }

    public class ChangeFolderDataBindingModel
    {
        [Display(Name = "Parent Id")]
        public string ParentId { get; set; }
        [Required]
        [Display(Name = "Folder Name")]
        public string FolderName { get; set; }
    }

    public class DeleteFolderBindingModel
    {
        [Display(Name = "Parent Id")]
        public string ParentId { get; set; }

        [Required]
        [Display(Name = "Folder Name")]
        public string FolderName { get; set; }
    }
}