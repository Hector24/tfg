﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Repository.API.Models
{
    public class File
    {
        [Display(Name = "Id")]
        public string id { get; set; }

        [Display(Name = "Parent Id")]
        public string ParentId { get; set; }

        [Required]
        [Display(Name = "File Name")]
        public string FileName { get; set; }

        [Required]
        [Display(Name = "Folder Owner")]
        public string Owner { get; set; }

        [Required]
        [Display(Name = "Format")]
        public string Format { get; set; }

        [Required]
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }

        [Required]
        [Display(Name = "Modification Date")]
        public DateTime ModificationDate { get; set; }
    }
}