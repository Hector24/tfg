﻿using System;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using System.Configuration;
using Neo4jClient;
using Neo4j.AspNet.Identity;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using Repository.API.Providers;
using Repository.API.Infrastructure;
using Repository.API;
using Microsoft.Owin.Security.OAuth;

[assembly: OwinStartup(typeof(Startup))]

namespace Repository.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration httpConfig = new HttpConfiguration();
            ConfigureOAuthTokenGeneration(app);
            ConfigureOAuthTokenConsumption(app);
            WebApiConfig.Register(httpConfig);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(httpConfig);
        }

        private void ConfigureNeo4j(IAppBuilder app)
        { //Configura y establece la conexión con la DB de Neo4j
            app.CreatePerOwinContext(() => {
                var url = ConfigurationManager.AppSettings["GraphDBUrl"];
                var user = ConfigurationManager.AppSettings["GraphDBUser"];
                var password = ConfigurationManager.AppSettings["GraphDBPassword"];
                var gc = new GraphClient(new Uri(url), user, password);
                gc.Connect();
                var gcw = new GraphClientWrapper(gc);
                return gcw;
            });
        }

        // Para obtener más información para configurar la autenticación, visite http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureOAuthTokenGeneration(IAppBuilder app)
        { //Configura el endpoint a donde el usuario manda sus credenciales para obtener el token de acceso
            // Configure el contexto de base de datos y el administrador de usuarios para usar una única instancia por solicitud
            ConfigureNeo4j(app);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            { 
                //For Dev enviroment only (on production should be AllowInsecureHttp = false)
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/oauth/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new CustomOAuthProvider(),
                AccessTokenFormat = new CustomJwtFormat("http://localhost:19434")
            };

            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
        }

        private void ConfigureOAuthTokenConsumption(IAppBuilder app)
        { 
            var issuer = "http://localhost:19434";
            string audienceId = ConfigurationManager.AppSettings["as:AudienceId"];
            byte[] audienceSecret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["as:AudienceSecret"]);

            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { audienceId },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, audienceSecret)
                    }
            });
        }
    }
}