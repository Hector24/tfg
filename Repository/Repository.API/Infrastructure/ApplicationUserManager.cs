﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Neo4j.AspNet.Identity;
using Repository.API.Models;

namespace Repository.API.Infrastructure
{
    public class ApplicationUserManager : UserManager<User> //Se encarga de gestionar los datos de los usuarios de la aplicación
    { //Sus funciones se usarán a la hora de comprobar si un usario existe o no, crear usuario, etc..
        public ApplicationUserManager(Neo4jUserStore<User> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        { //Sobreescribe la función de crear un usario en la app
            var manager = new ApplicationUserManager(new Neo4jUserStore<User>(context.Get<GraphClientWrapper>().GraphClient));
            // Configure la lógica de validación de nombres de usuario

            manager.UserValidator = new UserValidator<User>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure la lógica de validación de contraseñas
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            return manager;
        }
    }
}