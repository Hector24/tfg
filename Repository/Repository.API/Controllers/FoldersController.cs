﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Repository.API.Providers;
using Repository.API.Models;
using System;

namespace Repository.API.Controllers
{
    [RoutePrefix("api/folders")]
    public class FoldersController : BaseApiController
    {

        private GoogleDriveServiceProvider DriveProvider = new GoogleDriveServiceProvider();
        private string root_parentId = "0AMzZTPFHBE9AUk9PVA";

        [Authorize(Roles = "Admin, User with privileges")]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult CreateFolder(CreateFolderBindingModel createFolderModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)")
                .Where((Folder folder) => folder.FolderName == createFolderModel.FolderName)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if (results == null)
            {
                var service = this.DriveProvider.GetCredential();
                var fileMetadata = new Google.Apis.Drive.v3.Data.File()
                {
                    Name = createFolderModel.FolderName,
                    MimeType = "application/vnd.google-apps.folder"
                };
                var request = service.Files.Create(fileMetadata);
                request.Fields = "id";
                var file = request.Execute();

                var newFolder = new Folder()
                {
                    id = file.Id,
                    ParentId = this.root_parentId,
                    FolderName = createFolderModel.FolderName,
                    Owner = createFolderModel.Owner,
                    Files = new List<string>(),
                    CreationDate = DateTime.UtcNow,
                    ModificationDate = DateTime.UtcNow
                };

                WebApiConfig.GraphClient.Cypher
                    .Match("(user:User)")
                    .Where((Models.User user) => user.UserName == newFolder.Owner)
                    .Create("(user)-[:OWNER_OF]->(folder:Folder {folder})")
                    .WithParam("folder", newFolder)
                    .ExecuteWithoutResults();

                Uri locationHeader = new Uri(Url.Link("GetFolderByName", new { foldername = newFolder.FolderName }));

                return Created(locationHeader, TheModelFactory.CreateFolder(newFolder));
            }

            ModelState.AddModelError("", "Folder already exists");
            return BadRequest(ModelState);
        }

        [Authorize(Roles = "Admin, User with privileges")]
        [HttpPost]
        [Route("create/childfolder")]
        public IHttpActionResult CreateChildFolder(CreateFolderBindingModel createFolderModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)")
                .Where((Folder folder) => folder.FolderName == createFolderModel.FolderName && folder.ParentId == createFolderModel.ParentId)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if (results == null)
            {
                var service = this.DriveProvider.GetCredential();
                var fileMetadata = new Google.Apis.Drive.v3.Data.File()
                {
                    Name = createFolderModel.FolderName,
                    MimeType = "application/vnd.google-apps.folder",
                    Parents = new List<string>
                    {
                        createFolderModel.ParentId
                    }
                };
                var request = service.Files.Create(fileMetadata);
                request.Fields = "id";
                var file = request.Execute();

                var newFolder = new Folder()
                {
                    id = file.Id,
                    ParentId = createFolderModel.ParentId,
                    FolderName = createFolderModel.FolderName,
                    Owner = createFolderModel.Owner,
                    Files = new List<string>(),
                    CreationDate = DateTime.UtcNow,
                    ModificationDate = DateTime.UtcNow
                };

                WebApiConfig.GraphClient.Cypher
                    .Match("(parent:Folder),(user:User)")
                    .Where((Folder parent) => parent.id == createFolderModel.ParentId)
                    .AndWhere((Models.User user) => user.UserName == newFolder.Owner)
                    .Create("(parent)<-[:CHILD_OF]-(folder:Folder {folder})<-[:OWNER_OF]-(user)")
                    .WithParam("folder", newFolder)
                    .Set("parent.ModificationDate = {modificationdate}") //Modifica la fecha para poner la actual porque la carpeta ha sido modificada
                    .WithParam("modificationdate", DateTime.UtcNow)
                    .Set("parent.Files = parent.Files + {foldername}") //Añade el nombre de la carpeta hija que se ha creado a la lista de ficheros
                    .WithParam("foldername", newFolder.FolderName)
                    .ExecuteWithoutResults();

                Uri locationHeader = new Uri(Url.Link("GetFolderByName", new { foldername = newFolder.FolderName }));

                return Created(locationHeader, TheModelFactory.CreateFolder(newFolder));
            }

            ModelState.AddModelError("", "Folder already exists");
            return BadRequest(ModelState);
        }

        [AllowAnonymous]
        [Route("folder/{foldername}", Name = "GetFolderByName")]
        public IHttpActionResult GetFolderByName(string foldername)
        {

            var results = WebApiConfig.GraphClient.Cypher
                .Match("(folder:Folder)")
                .Where((Folder folder) => folder.FolderName == foldername)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if (results != null)
            {
                return Ok(this.TheModelFactory.CreateFolder(results));
            }

            ModelState.AddModelError("", "Folder doesn't exist");
            return BadRequest(ModelState);
        }

        [AllowAnonymous]
        [Route("parent/{parentId}")]
        public IHttpActionResult GetParentFolder(string parentId)
        {

            var result = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)")
                .Where((Folder folder) => folder.id == parentId)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if (result == null)
            {
                ModelState.AddModelError("", "Folder doesn't exist");
                return BadRequest(ModelState);
            }

            return Ok(result);
        }

        [Authorize(Roles = "Admin, User with privileges")]
        [HttpPost]
        [Route("delete")]
        public IHttpActionResult DeleteFolder(DeleteFolderBindingModel deleteFolderModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)")
                .Where((Folder folder) => folder.FolderName == deleteFolderModel.FolderName)
                .AndWhere((Folder folder) => folder.ParentId == deleteFolderModel.ParentId)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if (results == null)
            {
                ModelState.AddModelError("", "Folder doesn't exist");
                return BadRequest(ModelState);
            }

            //Esto solo se ejecuta si es una carpeta hijo
            if (results.ParentId != this.root_parentId)
            {
                //Se elimina del array de ficheros del padre el archivo a eliminar
                var parentfiles = WebApiConfig.GraphClient.Cypher
                    .Match("(parent:Folder)")
                    .Where((Folder parent) => parent.id == results.ParentId)
                    .Return(parent => parent.As<Folder>())
                    .Results.First();

                parentfiles.Files.Remove(deleteFolderModel.FolderName);

                WebApiConfig.GraphClient.Cypher
                        .Match("(parent:Folder)")
                        .Where((Folder parent) => parent.id == results.ParentId)
                        .Set("parent.ModificationDate = {modificationdate}")
                        .WithParam("modificationdate", DateTime.UtcNow)
                        .Set("parent.Files = {newFiles}")
                        .WithParam("newFiles", parentfiles.Files)
                        .ExecuteWithoutResults();
            }

            var service = this.DriveProvider.GetCredential();
            var request = service.Files.Delete(results.id);
            var file = request.Execute();

            if(results.Files.Count != 0) //Si la carpeta tiene elementos contenidos se borra con este metodo, si no con el que va a continuacion
            {
                WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)<-[:CHILD_OF*]-(child:Folder)<-[:CONTAINS]-(file:File)")
                .Where((Folder folder) => folder.FolderName == deleteFolderModel.FolderName)
                .DetachDelete("file")
                .ExecuteWithoutResultsAsync();

                WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)<-[:CHILD_OF*]-(child:Folder)")
                .Where((Folder folder) => folder.FolderName == deleteFolderModel.FolderName)
                .DetachDelete("child")
                .ExecuteWithoutResultsAsync();

                WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(user:User)-[:OWNER_OF]->(folder:Folder)<-[:CONTAINS]-(file:File)")
                .Where((Folder folder) => folder.FolderName == deleteFolderModel.FolderName)
                .DetachDelete("file,folder")
                .ExecuteWithoutResultsAsync();

                return Ok(results);
            }

            WebApiConfig.GraphClient.Cypher
                .Match("(folder:Folder)")
                .Where((Folder folder) => folder.FolderName == deleteFolderModel.FolderName)
                .AndWhere((Folder folder) => folder.ParentId == deleteFolderModel.ParentId)
                .DetachDelete("folder")
                .ExecuteWithoutResultsAsync();

            return Ok(results);
        }

        [Authorize(Roles = "Admin, User with privileges")]
        [Route("ChangeFolderData/{foldername}")]
        public IHttpActionResult ChangeFolderData(ChangeFolderDataBindingModel changeFolderModel, string foldername)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)")
                .Where((Folder folder) => folder.FolderName == foldername)
                .AndWhere((Folder folder) => folder.ParentId == changeFolderModel.ParentId)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if (results == null)
            {
                ModelState.AddModelError("", "Folder doesn't exist");
                return BadRequest(ModelState);
            }

            //Esto solo se ejecuta si es una carpeta hijo
            if (results.ParentId != this.root_parentId)
            {
                //Se elimina del array de ficheros del padre el nombre antiguo para más adelante añadirselo
                var parentfiles = WebApiConfig.GraphClient.Cypher
                    .Match("(parent:Folder)")
                    .Where((Folder parent) => parent.id == results.ParentId)
                    .Return(parent => parent.As<Folder>())
                    .Results.First();

                parentfiles.Files.Remove(foldername);

                //Se añade a los archivos del padre el nuevo nombre del hijo
                parentfiles.Files.Add(changeFolderModel.FolderName);
                WebApiConfig.GraphClient.Cypher
                        .Match("(parent:Folder)")
                        .Where((Folder parent) => parent.id == results.ParentId)
                        .Set("parent.ModificationDate = {modificationdate}")
                        .WithParam("modificationdate", DateTime.UtcNow)
                        .Set("parent.Files = {newFiles}")
                        .WithParam("newFiles", parentfiles.Files)
                        .ExecuteWithoutResults();
            }

            var service = this.DriveProvider.GetCredential();
            var request = service.Files.Get(results.id);
            var file = request.Execute();
            var fileMetadata = new Google.Apis.Drive.v3.Data.File()
            {
                Name = changeFolderModel.FolderName
            };
            var update = service.Files.Update(fileMetadata, file.Id);
            update.Execute();

            WebApiConfig.GraphClient.Cypher
                .Match("(folder:Folder)")
                .Where((Folder folder) => folder.FolderName == foldername)
                .AndWhere((Folder folder) => folder.ParentId == changeFolderModel.ParentId)
                .Set("folder.FolderName = {foldername}, folder.ModificationDate = {modificationdate}")
                .WithParam("foldername", changeFolderModel.FolderName)
                .WithParam("modificationdate", DateTime.UtcNow)
                .ExecuteWithoutResults();

            return Ok(results);
        }

    }
}