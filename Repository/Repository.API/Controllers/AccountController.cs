﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Repository.API.Models;
using System.Linq;

namespace Repository.API.Controllers
{
    [RoutePrefix("api/accounts")]
    public class AccountsController : BaseApiController
    {
        [AllowAnonymous]
        [HttpPost]
        [Route("create")]
        public async Task<IHttpActionResult> CreateUser(CreateUserBindingModel createUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new User()
            {
                UserName = createUserModel.Email,
                FirstName = createUserModel.FirstName,
                Email = createUserModel.Email,
                LastName = createUserModel.LastName,
            };

            IdentityResult addUserResult = await this.AppUserManager.CreateAsync(user, createUserModel.Password); //Pasa los datos enviados en el POST
            //a la funcion que crea el usuario en la DB

            IdentityResult result = await this.AppUserManager.AddToRoleAsync(user.Id, "User without privileges"); //Se le añade por defecto al rol de usuario sin privilegios

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }

            Uri locationHeader = new Uri(Url.Link("GetUserById", new { id = user.Id }));

            return Created(locationHeader, TheModelFactory.Create(user));
        }

        [Authorize(Roles = "Admin")]
        [Route("users")]
        public IHttpActionResult GetUsers()
        {
            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(user:User)")
                .Return(user => user.As<User>())
                .Results.ToList().Select(user => this.TheModelFactory.Create(user));

            return Ok(results);
        }

        [Authorize(Roles = "Admin")]
        [Route("user/{id:guid}", Name = "GetUserById")]
        public async Task<IHttpActionResult> GetUser(string Id)
        {
            var user = await this.AppUserManager.FindByIdAsync(Id);

            if (user != null) //Comprueba que el usuario existe, si lo hace lo devuelve, si no envia un error
            {
                return Ok(this.TheModelFactory.Create(user));
            }

            ModelState.AddModelError("", "User doesn't exist");
            return BadRequest(ModelState);

        }

        [Authorize]
        [Route("user/{username}")]
        public async Task<IHttpActionResult> GetUserByName(string username)
        {
            var user = await this.AppUserManager.FindByNameAsync(username);

            if (user != null)
            {
                return Ok(this.TheModelFactory.Create(user));
            }

            ModelState.AddModelError("", "User doesn't exist");
            return BadRequest(ModelState);

        }

        [Authorize]
        [Route("ChangePassword/{username}")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model, string username)
        {
            if (!ModelState.IsValid) //Comprueba que los datos recibidos se corresponden con los del modelo de datos para cambiar de contraseña 
            {
                return BadRequest(ModelState);
            }

            var user = this.AppUserManager.FindByEmail(username);
            IdentityResult result = await this.AppUserManager.ChangePasswordAsync(user.Id, model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok(result);
        }

        [Authorize]
        [Route("ChangeUserData/{username}")]
        public async Task<IHttpActionResult> ChangeUserData(ChangeUserDataBindingModel changeUserModel, string username)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await this.AppUserManager.FindByNameAsync(username);

            user.UserName = changeUserModel.Email;
            user.FirstName = changeUserModel.FirstName;
            user.Email = changeUserModel.Email;
            user.LastName = changeUserModel.LastName;

            IdentityResult result = await this.AppUserManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok(this.TheModelFactory.Create(user));
        }

        [Authorize(Roles = "Admin")]
        [Route("user/{username}")]
        public async Task<IHttpActionResult> DeleteUser(string username)
        {
            //Antes de borrar al usuario elimina todos los contenidos que haya podido generar en la aplicacion
            WebApiConfig.GraphClient.Cypher
            .OptionalMatch("(user:User)<-[relationship:MEMBER_OF]-(group)")
            .Where((User user) => user.UserName == username)
            .Delete("relationship")
            .ExecuteWithoutResultsAsync();

            WebApiConfig.GraphClient.Cypher
            .OptionalMatch("(user:User)-[:MANAGE]->(group:Group)")
            .Where((User user) => user.UserName == username)
            .DetachDelete("group")
            .ExecuteWithoutResultsAsync();

            WebApiConfig.GraphClient.Cypher
            .OptionalMatch("(user:User)-[:OWNER_OF]->(folder:Folder)<-[:CHILD_OF*]-(child:Folder)<-[:CONTAINS]-(file:File)")
            .Where((User user) => user.UserName == username)
            .DetachDelete("file")
            .ExecuteWithoutResultsAsync();

            WebApiConfig.GraphClient.Cypher
            .OptionalMatch("(user:User)-[:OWNER_OF]->(folder:Folder)<-[:CHILD_OF*]-(child:Folder)")
            .Where((User user) => user.UserName == username)
            .DetachDelete("child")
            .ExecuteWithoutResultsAsync();

            WebApiConfig.GraphClient.Cypher
            .OptionalMatch("(user:User)-[:OWNER_OF]->(folder:Folder)<-[:CONTAINS]-(file:File)")
            .Where((User user) => user.UserName == username)
            .DetachDelete("file,folder")
            .ExecuteWithoutResultsAsync();

            var appUser = await this.AppUserManager.FindByNameAsync(username);

            if (appUser != null)
            {

                IdentityResult result = await this.AppUserManager.DeleteAsync(appUser);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }

                return Ok(result);

            }

            ModelState.AddModelError("", "User doesn't exist");
            return BadRequest(ModelState);
        }

        [Authorize(Roles= "Admin")]
        [Route("ChangeUserType")]
        public async Task<IHttpActionResult> ChangeUserType(ChangeUserTypeBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User user = await this.AppUserManager.FindByNameAsync(model.UserName);

            if (user == null)
            {
                ModelState.AddModelError("", "User doesn't exist");
                return BadRequest(ModelState);
            }

            var currentRoles = await this.AppUserManager.GetRolesAsync(user.Id); //Obtiene el rol actual del usuario 

            IdentityResult removeResult = await this.AppUserManager.RemoveFromRolesAsync(user.Id, currentRoles.ToArray()); //Lo elimina

            if (!removeResult.Succeeded)
            {
                ModelState.AddModelError("", "Failed to remove user roles");
                return BadRequest(ModelState);
            }

            IdentityResult result = await this.AppUserManager.AddToRoleAsync(user.Id,model.Role); //Lo sustituye por el nuevo que se envio en la peticion POST

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok(result);
        }
    }
}
