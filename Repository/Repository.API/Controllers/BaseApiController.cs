﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Repository.API.Infrastructure;
using Repository.API.Models;
using System.Net.Http;
using System.Web.Http;

namespace Repository.API.Controllers
{
    public class BaseApiController : ApiController
    {

        private ModelFactory _modelFactory;
        private ApplicationUserManager _AppUserManager = null;

        protected ApplicationUserManager AppUserManager //Obtiene una instancia del manager de usuarios de la carpeta de Infrastructure
        {
            get
            {
                return _AppUserManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public BaseApiController()
        {
        }

        protected ModelFactory TheModelFactory //Propiedad de solo lectura que devuelve una instancia de ModelFactory
        { //que se usa para manipular y mostrar los datos que llegan de la DB
            get
            {
                if (_modelFactory == null)
                {
                    _modelFactory = new ModelFactory(this.Request, this.AppUserManager);
                }
                return _modelFactory;
            }
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        { //Se utiliza para personalizar el mensaje de error arrojado por la aplicacion
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}