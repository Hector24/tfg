﻿using Repository.API.Models;
using System.Linq;
using System.Web.Http;

namespace Repository.API.Controllers
{
    [RoutePrefix("api/access")]
    public class AccessController : BaseApiController
    {

        [Authorize]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult CreateAccess(AccessBindingModel createAccessModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var groupresults = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(group:Group)")
                .Where((Group group) => group.GroupName == createAccessModel.GroupName)
                .Return(group => group.As<Group>())
                .Results.First();

            var folderresults = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)")
                .Where((Folder folder) => folder.id == createAccessModel.FolderId)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if (groupresults != null && folderresults != null)
            {
                var newAccessData = new Access()
                {
                    AccessPermission = createAccessModel.AccessPermission,
                    AccessMode = createAccessModel.AccessMode,
                };

                var relationship = WebApiConfig.GraphClient.Cypher
                   .OptionalMatch("(group)-[rel:HAS_ACCESS]->(folder)")
                   .Where((Folder folder) => folder.id == createAccessModel.FolderId)
                   .AndWhere((Group group) => group.GroupName == createAccessModel.GroupName)
                   .Return(rel => rel.As<Access>())
                   .Results.First();

                if(relationship == null)
                {
                    WebApiConfig.GraphClient.Cypher
                    .Match("(group:Group),(folder:Folder)")
                    .Where((Folder folder) => folder.id == createAccessModel.FolderId)
                    .AndWhere((Group group) => group.GroupName == createAccessModel.GroupName)
                    .CreateUnique("(folder)<-[:HAS_ACCESS {Access}]-(group)")
                    .WithParam("Access", newAccessData)
                    .ExecuteWithoutResults();

                    return Ok(newAccessData);
                }

                ModelState.AddModelError("", "Access modes & permit had already been assigned, if you want to realize some changes, please direct to modify access modes & permit");
                return BadRequest(ModelState);

            }

            ModelState.AddModelError("", "Group or Folder doesn't exists");
            return BadRequest(ModelState);
        }

        [Authorize]
        [Route("content/owned/{username}")]
        public IHttpActionResult GetAccess(string username)
        {
            //Obtiene las carpetas de las que es propietario para indexar en Angular las que no tienen acceso concedido para que el propietario pueda gestionarlo
            var results = WebApiConfig.GraphClient.Cypher
                   .Match("(user:User)-[:OWNER_OF]->(folder:Folder)")
                   .Where((User user) => user.UserName == username)
                   .Return(folder => folder.As<Folder>())
                   .Results.ToList().Select(folder => this.TheModelFactory.CreateFolder(folder));

            return Ok(results);
        }

        [Authorize]
        [Route("content/{username}")]
        public IHttpActionResult GetContent(string username)
        {

            var resultInheritance = WebApiConfig.GraphClient.Cypher
                .Match("(user:User)-[membership]->(:Group)-[access:HAS_ACCESS]->(folder:Folder)")
                .Where((User user) => user.UserName == username)
                .AndWhere((Access access) => access.AccessPermission == "Access with inheritance")
                .ReturnDistinct(folder => folder.As<Folder>()) //ReturnDistinct es para que si varios grupos tienen acceso a una misma carpeta de la misma manera no la duplique
                .Results.ToList().Select(folder => this.TheModelFactory.CreateFolder(folder));

            var resultNoInheritance = WebApiConfig.GraphClient.Cypher
                .Match("(user:User)-[membership]->(:Group)-[access:HAS_ACCESS]->(folder:Folder)")
                .Where((User user) => user.UserName == username)
                .AndWhere((Access access) => access.AccessPermission == "Access without inheritance")
                .ReturnDistinct(folder => folder.As<Folder>())
                .Results.ToList().Select(folder => this.TheModelFactory.CreateFolder(folder));

            var resultDenied = WebApiConfig.GraphClient.Cypher
                .Match("(user:User)-[membership]->(:Group)-[access:HAS_ACCESS]->(folder:Folder)")
                .Where((User user) => user.UserName == username)
                .AndWhere((Access access) => access.AccessPermission == "Access denied")
                .ReturnDistinct(folder => folder.As<Folder>())
                .Results.ToList().Select(folder => this.TheModelFactory.CreateFolder(folder));

            var resultChildDenied = WebApiConfig.GraphClient.Cypher
                .Match("(user:User)-[membership]->(:Group)-[access:HAS_ACCESS]->(:Folder)<-[:CHILD_OF*]-(folder:Folder)")
                .Where((User user) => user.UserName == username)
                .AndWhere((Access access) => access.AccessPermission == "Access denied")
                .ReturnDistinct(folder => folder.As<Folder>())
                .Results.ToList().Select(folder => this.TheModelFactory.CreateFolder(folder));

            resultDenied = resultDenied.Concat(resultChildDenied);

            //Elimina las carpetas denegadas de las que tienen acceso con herencia
            foreach (var x in resultInheritance)
            {
                foreach (var y in resultDenied)
                {
                    if(x.FolderName == y.FolderName)
                    {
                        resultInheritance = resultInheritance.Where(u => u.FolderName != y.FolderName).ToList();
                    }
                }
            }

            //Elimina las carpetas sin herencia de las que tienen acceso con herencia para evitar duplicaciones
            foreach (var x in resultInheritance)
            {
                foreach (var y in resultNoInheritance)
                {
                    if (x.FolderName == y.FolderName)
                    {
                        resultInheritance = resultInheritance.Where(u => u.FolderName != y.FolderName).ToList();
                    }
                }
            }

            resultInheritance = resultInheritance.Concat(resultNoInheritance);

            return Ok(resultInheritance);
        }

        [Authorize]
        [HttpPost]
        [Route("content/child")]
        public IHttpActionResult GetChildContent(ChildContentBindingModel childcontent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var checkAccess = WebApiConfig.GraphClient.Cypher //Obtiene si esa carpeta tiene aunque solo sea una relacion de acceso, si la tiene obtiene su contenido
                .OptionalMatch("(user:User)-[membership]->(:Group)-[access:HAS_ACCESS]->(folder:Folder)") //No hay que preocuparse por los denegados porque estos no se indexan
                .Where((User user) => user.UserName == childcontent.UserName)
                .AndWhere((Folder folder) => folder.id == childcontent.FolderId)
                .Return(access => access.As<Access>())
                .Results.First();

            if(checkAccess == null)
            {
                //Comprueba que la carpeta padre de la que se quiere observar el contenido tiene acceso sin herencia y con herencia 
                //En caso de tenr solo la 1a no se podría visualzar ese contenido
                var checkNoInheritance = WebApiConfig.GraphClient.Cypher
                    .OptionalMatch("(child:Folder)-[:CHILD_OF*]->(folder:Folder)<-[access:HAS_ACCESS]-(:Group)<-[membership]-(user:User)")
                    .Where((User user) => user.UserName == childcontent.UserName)
                    .AndWhere((Folder child) => child.id == childcontent.FolderId)
                    .AndWhere((Access access) => access.AccessPermission == "Access without inheritance")
                    .Return(folder => folder.As<Folder>())
                    .Results.Last();

                var checkInheritance = WebApiConfig.GraphClient.Cypher
                    .OptionalMatch("(child:Folder)-[:CHILD_OF*]->(folder:Folder)<-[access:HAS_ACCESS]-(:Group)<-[membership]-(user:User)")
                    .Where((User user) => user.UserName == childcontent.UserName)
                    .AndWhere((Folder child) => child.id == childcontent.FolderId)
                    .AndWhere((Access access) => access.AccessPermission == "Access with inheritance")
                    .Return(folder => folder.As<Folder>())
                    .Results.Last();

                if (checkNoInheritance != null && checkInheritance == null)
                {
                    ModelState.AddModelError("", "You don't have access to this point of the hierarchy");
                    return BadRequest(ModelState);
                }
            }

            //Se obtienen todos los hijos de esa carpeta 
            var results = WebApiConfig.GraphClient.Cypher
                .Match("(folder:Folder)<-[:CHILD_OF]-(child:Folder)")
                .Where((Folder folder) => folder.id == childcontent.FolderId)
                .Return(child => child.As<Folder>())
                .Results.ToList().Select(child => this.TheModelFactory.CreateFolder(child));

            var childsDenied = WebApiConfig.GraphClient.Cypher
                .Match("(folder:Folder)<-[:CHILD_OF]-(child:Folder)<-[access:HAS_ACCESS]-(:Group)<-[membership]-(user:User)")
                .Where((User user) => user.UserName == childcontent.UserName)
                .AndWhere((Folder folder) => folder.id == childcontent.FolderId)
                .AndWhere((Access access) => access.AccessPermission == "Access denied")
                .Return(child => child.As<Folder>())
                .Results.ToList().Select(child => this.TheModelFactory.CreateFolder(child));

            //Elimina las carpetas denegadas de las carpetas hijo para que solo se muestren a las que tiene acceso
            foreach (var x in results)
            {
                foreach (var y in childsDenied)
                {
                    if (x.FolderName == y.FolderName)
                    {
                        results = results.Where(u => u.FolderName != y.FolderName).ToList();
                    }
                }
            }

            return Ok(results);
        }

        [Authorize]
        [HttpPost]
        [Route("get/access")]
        public IHttpActionResult GetAccess(GetAccessBindingModel getAccessModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var groupresults = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(group:Group)")
                .Where((Group group) => group.GroupName == getAccessModel.GroupName)
                .Return(group => group.As<Group>())
                .Results.First();

            var folderresults = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)")
                .Where((Folder folder) => folder.id == getAccessModel.FolderId)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if (groupresults == null && folderresults == null)
            {
                ModelState.AddModelError("", "Folder or Group doesn't exist");
                return BadRequest(ModelState);
            }

            //Obtiene si el grupo tiene acceso concedido a esa carpeta
            var relationship = WebApiConfig.GraphClient.Cypher
                   .OptionalMatch("(group)-[rel:HAS_ACCESS]->(folder)")
                   .Where((Folder folder) => folder.id == getAccessModel.FolderId)
                   .AndWhere((Group group) => group.GroupName == getAccessModel.GroupName)
                   .Return(rel => rel.As<Access>())
                   .Results.First();

            if (relationship == null)
            {
                ModelState.AddModelError("", "This group doesn't have an access established to this folder, please grant access first.");
                return BadRequest(ModelState);
            }

            return Ok(relationship);
        }

        [Authorize]
        [HttpPost]
        [Route("modify")]
        public IHttpActionResult ModifyAccess(AccessBindingModel modifyAccessModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var groupresults = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(group:Group)")
                .Where((Group group) => group.GroupName == modifyAccessModel.GroupName)
                .Return(group => group.As<Group>())
                .Results.First();

            var folderresults = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)")
                .Where((Folder folder) => folder.id == modifyAccessModel.FolderId)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if (groupresults == null && folderresults == null)
            {
                ModelState.AddModelError("", "Folder or Group doesn't exist");
                return BadRequest(ModelState);
            }
            
            //Obtiene si el grupo tiene acceso concedido a esa carpeta
            var relationship = WebApiConfig.GraphClient.Cypher
                   .OptionalMatch("(group)-[rel:HAS_ACCESS]->(folder)")
                   .Where((Folder folder) => folder.id == modifyAccessModel.FolderId)
                   .AndWhere((Group group) => group.GroupName == modifyAccessModel.GroupName)
                   .Return(rel => rel.As<Access>())
                   .Results.First();

            if(relationship == null)
            {
                ModelState.AddModelError("", "This group doesn't have an access established to this folder, please grant access first.");
                return BadRequest(ModelState);
            }

            //Cambia la lista de modos de acceso en la base de datos por la nueva
            var results = WebApiConfig.GraphClient.Cypher
                   .Match("(group)-[rel:HAS_ACCESS]->(folder)")
                   .Where((Folder folder) => folder.id == modifyAccessModel.FolderId)
                   .AndWhere((Group group) => group.GroupName == modifyAccessModel.GroupName)
                   .Set("rel.AccessMode = {AccessMode}")
                   .WithParam("AccessMode", modifyAccessModel.AccessMode)
                   .Set("rel.AccessPermission = {AccessPermission}")
                   .WithParam("AccessPermission", modifyAccessModel.AccessPermission)
                   .Return(rel => rel.As<Access>())
                   .Results.First();

            return Ok(results);
        }

        [Authorize]
        [HttpPost]
        [Route("delete")]
        public IHttpActionResult DeleteAccess(GetAccessBindingModel deleteFolderModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var groupresults = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(group:Group)")
                .Where((Group group) => group.GroupName == deleteFolderModel.GroupName)
                .Return(group => group.As<Group>())
                .Results.First();

            var folderresults = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)")
                .Where((Folder folder) => folder.id == deleteFolderModel.FolderId)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if (groupresults == null && folderresults == null)
            {
                ModelState.AddModelError("", "Folder or Group doesn't exist");
                return BadRequest(ModelState);
            }

            WebApiConfig.GraphClient.Cypher
                   .OptionalMatch("(group)-[relationship:HAS_ACCESS]->(folder)")
                   .Where((Folder folder) => folder.id == deleteFolderModel.FolderId)
                   .AndWhere((Group group) => group.GroupName == deleteFolderModel.GroupName)
                   .Delete("relationship")
                   .ExecuteWithoutResults();

            return Ok(groupresults);
        }

        [Authorize]
        [HttpPost]
        [Route("get/accessmodes")]
        public IHttpActionResult GetAccessModes(ChildContentBindingModel getaccessmodes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var checkAccess = WebApiConfig.GraphClient.Cypher //Obtiene si esa carpeta tiene aunque solo sea una relacion de acceso, esta asi porque con el array falla
                .OptionalMatch("(user:User)-[membership]->(:Group)-[access:HAS_ACCESS]->(folder:Folder)")
                .Where((User user) => user.UserName == getaccessmodes.UserName)
                .AndWhere((Folder folder) => folder.id == getaccessmodes.FolderId)
                .Return(access => access.As<Access>())
                .Results.First();

            if(checkAccess == null) //Si results no obtiene ninguna relacion se busca en sentido ascendente en la jerarquia
            {
                var parentFolder = WebApiConfig.GraphClient.Cypher
                    .OptionalMatch("(child:Folder)-[:CHILD_OF*]->(folder:Folder)<-[access:HAS_ACCESS]-(:Group)<-[membership]-(user:User)") //Si da problemas quitar optional
                    .Where((User user) => user.UserName == getaccessmodes.UserName)
                    .AndWhere((Folder child) => child.id == getaccessmodes.FolderId)
                    .Return(folder => folder.As<Folder>())
                    .Results.Last();

                if (parentFolder == null) //Si se esta obteniendo contenido de carpetas de las que el usuario es propietario pero no tiene acceso definido se muestra este mensaje
                {
                    ModelState.AddModelError("", "This folder doesn't have an access established, please grant access first.");
                    return BadRequest(ModelState);
                }

                var parentResults = WebApiConfig.GraphClient.Cypher
                    .Match("(user:User)-[membership]->(:Group)-[access:HAS_ACCESS]->(folder:Folder)")
                    .Where((User user) => user.UserName == getaccessmodes.UserName)
                    .AndWhere((Folder folder) => folder.id == parentFolder.id)
                    .Return(access => access.As<Access>())
                    .Results.ToList().Select(access => this.TheModelFactory.CreateAccess(access));

                return Ok(parentResults);
            }

            var results = WebApiConfig.GraphClient.Cypher //Si esa carpeta ya tiene relación de acceso obtiene todos los modos de acceso posibles
                .Match("(user:User)-[membership]->(:Group)-[access:HAS_ACCESS]->(folder:Folder)")
                .Where((User user) => user.UserName == getaccessmodes.UserName)
                .AndWhere((Folder folder) => folder.id == getaccessmodes.FolderId)
                .Return(access => access.As<Access>())
                .Results.ToList().Select(access => this.TheModelFactory.CreateAccess(access));

            return Ok(results);
        }
    }
}