﻿using Repository.API.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Repository.API.Controllers
{
    [RoutePrefix("api/groups")]
    public class GroupsController : BaseApiController
    {
        [Authorize(Roles = "Admin, User with privileges")]
        [HttpPost]
        [Route("create")]
        public IHttpActionResult CreateGroup(CreateGroupBindingModel createGroupModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var newGroup = new Group()
            {
                GroupName = createGroupModel.GroupName,
                Owner = createGroupModel.Owner,
            };

            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(group:Group)")
                .Where((Group group) => group.GroupName == newGroup.GroupName)
                .Return(group => group.As<Group>())
                .Results.First();

            if(results == null)
            {
                WebApiConfig.GraphClient.Cypher
                    .Match("(user:User)")
                    .Where((User user) => user.UserName == newGroup.Owner)
                    .Create("(user)-[:MANAGE]->(group:Group {group})")
                    .WithParam("group", newGroup)
                    .ExecuteWithoutResults();

                Uri locationHeader = new Uri(Url.Link("GetGroupByName", new { groupname = newGroup.GroupName }));

                return Created(locationHeader, TheModelFactory.CreateGroup(newGroup));
            }

            ModelState.AddModelError("", "Group already exists");
            return BadRequest(ModelState);
        }

        [AllowAnonymous]
        [Route("group/{groupname}", Name = "GetGroupByName")]
        public IHttpActionResult GetGroupByName(string groupname)
        {
            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(group:Group)")
                .Where((Group group) => group.GroupName == groupname)
                .Return(group => group.As<Group>())
                .Results.First();

            if (results != null)
            {
                return Ok(this.TheModelFactory.CreateGroup(results));
            }

            ModelState.AddModelError("", "Group doesn't exist");
            return BadRequest(ModelState);

        }

        [Authorize(Roles = "Admin")]
        [Route("list")]
        public IHttpActionResult GetGroups()
        {
            var result = WebApiConfig.GraphClient.Cypher
                .Match("(group:Group)")
                .Return(group => group.As<Group>())
                .Results.ToList().Select(group => this.TheModelFactory.CreateGroup(group));

            return Ok(result);
        }

        [Authorize]
        [Route("user/list/{username}")]
        public async Task<IHttpActionResult> GetUserGroups(string username)
        {

            var member = await this.AppUserManager.FindByNameAsync(username);

            if(member != null)
            {
                var result = WebApiConfig.GraphClient.Cypher
                    .Match("(group:Group)<-[relationship]-(user:User)")
                    .Where((User user) => user.UserName == username)
                    .Return(group => group.As<Group>())
                    .Results.ToList().Select(group => this.TheModelFactory.CreateGroup(group));

                return Ok(result);
            }

            ModelState.AddModelError("", "User doesn't exist");
            return BadRequest(ModelState);
        }

        [Authorize]
        [Route("list/members/{groupname}")]
        public IHttpActionResult GetMembersList(string groupname)
        {
            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(group:Group)")
                .Where((Group group) => group.GroupName == groupname)
                .Return(group => group.As<Group>())
                .Results.First();

            if (results == null)
            {
                ModelState.AddModelError("", "Group doesn't exist");
                return BadRequest(ModelState);
            }
            
            var members = WebApiConfig.GraphClient.Cypher
                .Match("(group:Group)<-[relationship]-(user:User)")
                .Where((Group group) => group.GroupName == groupname)
                .Return(user => user.As<User>())
                .Results.ToList().Select(user => this.TheModelFactory.Create(user));

            return Ok(members);
        }

        [Authorize(Roles = "Admin, User with privileges")]
        [Route("delete/{groupname}")]
        public IHttpActionResult DeleteGroup(string groupname)
        {

            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(group:Group)")
                .Where((Group group) => group.GroupName == groupname)
                .Return(group => group.As<Group>())
                .Results.First();

            if (results == null)
            {
                ModelState.AddModelError("", "Group doesn't exist");
                return BadRequest(ModelState);
            }

            WebApiConfig.GraphClient.Cypher
                .Match("(group:Group)")
                .Where((Group group) => group.GroupName == groupname)
                .DetachDelete("group")
                .ExecuteWithoutResultsAsync();

            return Ok(results);
        }

        [Authorize(Roles = "Admin, User with privileges")]
        [Route("AddToGroup")]
        public async Task<IHttpActionResult> AddToGroup(AddToGroupBindingModel AddToGroupModel)
        {
            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(group:Group)")
                .Where((Group group) => group.GroupName == AddToGroupModel.GroupName)
                .Return(group => group.As<Group>())
                .Results.First();

            var member = await this.AppUserManager.FindByNameAsync(AddToGroupModel.UserName);

            if (member != null && results != null)
            {
                WebApiConfig.GraphClient.Cypher
                   .Match("(user:User)","(group:Group)")
                   .Where((User user) => user.UserName == AddToGroupModel.UserName)
                   .AndWhere((Group group) => group.GroupName == AddToGroupModel.GroupName)
                   .CreateUnique("(user)-[:MEMBER_OF]->(group)")
                   .ExecuteWithoutResults();

                return Ok(results);
            }

            ModelState.AddModelError("", "User or Group doesn't exist");
            return BadRequest(ModelState);

        }

        [Authorize(Roles = "Admin, User with privileges")]
        [HttpDelete]
        [Route("RemoveFromGroup")]
        public async Task<IHttpActionResult> RemoveFromGroup(AddToGroupBindingModel AddToGroupModel)
        {
            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(group:Group)")
                .Where((Group group) => group.GroupName == AddToGroupModel.GroupName)
                .Return(group => group.As<Group>())
                .Results.First();

            var member = await this.AppUserManager.FindByNameAsync(AddToGroupModel.UserName);

            if (member != null && results != null)
            {
                WebApiConfig.GraphClient.Cypher
                   .Match("(user)-[relationship:MEMBER_OF]->(group)")
                   .Where((User user) => user.UserName == AddToGroupModel.UserName)
                   .AndWhere((Group group) => group.GroupName == AddToGroupModel.GroupName)
                   .Delete("relationship")
                   .ExecuteWithoutResults();

                return Ok(results);
            }

            ModelState.AddModelError("", "User or Group doesn't exist");
            return BadRequest(ModelState);

        }
    }
}
