﻿using System.Linq;
using System.Web.Http;
using Repository.API.Providers;
using Repository.API.Models;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using Google.Apis.Drive.v3;
using System.Collections.Generic;

namespace Repository.API.Controllers
{
    public class ValidateMimeMultipartContentFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {

        }

    }

    [RoutePrefix("api/files")]
    public class FileController : BaseApiController
    {

        private GoogleDriveServiceProvider DriveProvider = new GoogleDriveServiceProvider();
        private string root_parentId = "0AMzZTPFHBE9AUk9PVA";

        //Funcion que obtiene la extension del archivo para GoogleDrive
        private static string GetMimeType(string FileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(FileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }

        [Authorize(Roles = "Admin, User with privileges")]
        [HttpPost]
        [Route("upload/dB")]
        public IHttpActionResult UploadFiledB(CreateFileBindingModel createFileModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(file:File)")
                .Where((Models.File file) => file.FileName == createFileModel.FileName && file.ParentId == createFileModel.ParentId)
                .Return(file => file.As<Models.File>())
                .Results.First();

            if (results == null)
            {

                var newFile = new Models.File()
                {
                    ParentId = createFileModel.ParentId,
                    FileName = createFileModel.FileName,
                    Owner = createFileModel.Owner,
                    Format = GetMimeType(createFileModel.FileName),
                    CreationDate = DateTime.UtcNow,
                    ModificationDate = DateTime.UtcNow
                };

                WebApiConfig.GraphClient.Cypher
                    .Match("(parent:Folder),(user:User)")
                    .Where((Folder parent) => parent.id == createFileModel.ParentId)
                    .AndWhere((Models.User user) => user.UserName == newFile.Owner)
                    .Create("(parent)<-[:CONTAINS]-(file:File {file})<-[:OWNER_OF]-(user)")
                    .WithParam("file", newFile)
                    .Set("parent.ModificationDate = {modificationdate}")
                    .WithParam("modificationdate", DateTime.UtcNow)
                    .Set("parent.Files = parent.Files + {filename}")
                    .WithParam("filename", newFile.FileName)
                    .ExecuteWithoutResults();

                Uri locationHeader = new Uri(Url.Link("GetFileByName", new { filename = newFile.FileName }));

                return Created(locationHeader, TheModelFactory.CreateFile(newFile));
            }

            ModelState.AddModelError("", "File already exists");
            return BadRequest(ModelState);
        }

        [Authorize(Roles = "Admin, User with privileges")]
        [HttpPost]
        [Route("upload/drive/{parentId}")]
        public IHttpActionResult UploadFileDrive(string parentId)
        {
            Google.Apis.Drive.v3.Data.File result = new Google.Apis.Drive.v3.Data.File(); //Se declara la variable vacia para poder devolverla en la respuesta Ok
            HttpResponseMessage response = new HttpResponseMessage();
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                foreach (string file in httpRequest.Files)
                {
                    //Guarda el archivo en la carpeta local para subirlo usando esa ruta a GDrive
                    var postedFile = httpRequest.Files[file];
                    var filePath = HttpContext.Current.Server.MapPath("~/App_Data/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);

                    //Esta rutina seria como subir un archivo a Google Drive
                    var service = this.DriveProvider.GetCredential();
                    var fileMetadata = new Google.Apis.Drive.v3.Data.File()
                    {
                        Name = postedFile.FileName,
                        Parents = new List<string>
                        {
                            parentId
                        }
                    };

                    FilesResource.CreateMediaUpload request;
                    using (var stream = new System.IO.FileStream(filePath,
                                        System.IO.FileMode.Open))
                    {
                        request = service.Files.Create(fileMetadata, stream, GetMimeType(postedFile.FileName)); //GetMime es la funcion que obtiene el tipo de archivo para GDrive
                        request.Fields = "id";
                        request.Upload();
                    }
                    result = request.ResponseBody;

                    WebApiConfig.GraphClient.Cypher
                        .OptionalMatch("(uploaded:File)")
                        .Where((Models.File uploaded) => uploaded.FileName == postedFile.FileName && uploaded.ParentId == parentId)
                        .Set("uploaded.id = {id}")
                        .WithParam("id", result.Id)
                        .ExecuteWithoutResults();
                }
            }
            else
            {
                ModelState.AddModelError("", "An error occurred uploading your file, please retry");
                return BadRequest(ModelState);
            }

            return Ok(result);
        }

        [AllowAnonymous]
        [Route("file/{filename}", Name = "GetFileByName")]
        public IHttpActionResult GetFileByName(string filename)
        {

            var results = WebApiConfig.GraphClient.Cypher
                .Match("(file:File)")
                .Where((Models.File file) => file.FileName == filename)
                .Return(file => file.As<Models.File>())
                .Results.First();

            if (results != null)
            {
                return Ok(this.TheModelFactory.CreateFile(results));
            }

            ModelState.AddModelError("", "File doesn't exist");
            return BadRequest(ModelState);
        }

        [Authorize(Roles = "Admin, User with privileges")]
        [HttpPost]
        [Route("update/{parentId}")]
        public IHttpActionResult UpdateFile(string parentId)
        {

            Google.Apis.Drive.v3.Data.File result = new Google.Apis.Drive.v3.Data.File(); //Se declara la variable vacia para poder devolverla en la respuesta Ok
            HttpResponseMessage response = new HttpResponseMessage();
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                foreach (string file in httpRequest.Files)
                {

                    //Guarda el archivo en la carpeta local para subirlo usando esa ruta a GDrive
                    var postedFile = httpRequest.Files[file];
                    var filePath = HttpContext.Current.Server.MapPath("~/App_Data/" + postedFile.FileName);
                    postedFile.SaveAs(filePath);

                    var results = WebApiConfig.GraphClient.Cypher
                        .OptionalMatch("(update:File)")
                        .Where((Models.File update) => update.FileName == postedFile.FileName && update.ParentId == parentId)
                        .Return(update => update.As<Models.File>())
                        .Results.First();

                    if (results == null)
                    {
                        ModelState.AddModelError("", "File doesn't exist");
                        return BadRequest(ModelState);
                    }

                    //Esta rutina seria para subir un archivo a Google Drive
                    var service = this.DriveProvider.GetCredential();
                    var fileMetadata = new Google.Apis.Drive.v3.Data.File()
                    {
                        Name = postedFile.FileName,
                    };

                    FilesResource.UpdateMediaUpload request;
                    using (var stream = new System.IO.FileStream(filePath,
                                        System.IO.FileMode.Open))
                    {
                        request = service.Files.Update(result, results.id, stream, GetMimeType(postedFile.FileName)); //GetMime es la funcion que obtiene el tipo de archivo para GDrive
                        request.Fields = "id";
                        request.Upload();
                    }
                    result = request.ResponseBody;

                    WebApiConfig.GraphClient.Cypher
                        .Match("(update:File)")
                        .Where((Models.File update) => update.FileName == postedFile.FileName && update.ParentId == parentId)
                        .Set("update.ModificationDate = {modificationdate}")
                        .WithParam("modificationdate", DateTime.UtcNow)
                        .ExecuteWithoutResults();
                }
            }
            else
            {
                ModelState.AddModelError("", "An error occurred updating your file, please retry");
                return BadRequest(ModelState);
            }

            return Ok(result);
        }

        [Authorize]
        [Route("get/files/{folderId}")]
        public IHttpActionResult GetFiles(string folderId)
        {
            var folderresults = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(folder:Folder)")
                .Where((Folder folder) => folder.id == folderId)
                .Return(folder => folder.As<Folder>())
                .Results.First();

            if(folderresults == null)
            {
                ModelState.AddModelError("", "Folder doesn't exist");
                return BadRequest(ModelState);
            }

            //Obtiene todos los ficheros contenidos en dicha carpeta
            var results = WebApiConfig.GraphClient.Cypher
                .Match("(folder:Folder)<-[:CONTAINS]-(file:File)")
                .Where((Folder folder) => folder.id == folderId)
                .Return(file => file.As<Models.File>())
                .Results.ToList().Select(file => this.TheModelFactory.CreateFile(file));

            return Ok(results);
        }

        [Authorize]
        [HttpGet]
        [Route("download/{Id}")]
        public IHttpActionResult DownloadFile(string Id)
        {
            //Esta rutina seria para descargar un archivo de Google Drive
            var service = this.DriveProvider.GetCredential();
            string pageToken = null;
            var filePath = HttpContext.Current.Server.MapPath("~/");
            //Obtiene la lista de ficheros en GDrive
            List<Google.Apis.Drive.v3.Data.File> filesList = new List<Google.Apis.Drive.v3.Data.File>();
            do
            {
                var request = service.Files.List();
                request.Spaces = "drive";
                request.Fields = "nextPageToken, files(id, name, createdTime, webContentLink, fullFileExtension, fileExtension, modifiedTime, parents)";
                request.PageToken = pageToken;
                var result = request.Execute();
                foreach (var file in result.Files)
                {
                    filesList.Add(file);
                }
                pageToken = result.NextPageToken;
            } while (pageToken != null);

            //Selecciona el archivo deseado y devuelve su enlace de descarga
            foreach (var file in filesList)
            {
                if(file.Id == Id)
                {
                    return Ok(file);
                }
            }

            ModelState.AddModelError("", "File doesn't exist");
            return BadRequest(ModelState);
        }

        [Authorize(Roles = "Admin, User with privileges")]
        [HttpPost]
        [Route("delete")]
        public IHttpActionResult DeleteFolder(DeleteFileBindingModel deleteFileModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var results = WebApiConfig.GraphClient.Cypher
                .OptionalMatch("(file:File)")
                .Where((Models.File file) => file.FileName == deleteFileModel.FileName && file.ParentId == deleteFileModel.ParentId)
                .Return(file => file.As<Models.File>())
                .Results.First();

            if (results == null)
            {
                ModelState.AddModelError("", "File doesn't exist");
                return BadRequest(ModelState);
            }

            //Esto solo se ejecuta si esta contenido en una carpeta hijo
            if (results.ParentId != this.root_parentId)
            {
                //Se elimina del array de ficheros del padre el archivo a eliminar
                var parentfiles = WebApiConfig.GraphClient.Cypher
                    .Match("(parent:Folder)")
                    .Where((Folder parent) => parent.id == results.ParentId)
                    .Return(parent => parent.As<Folder>())
                    .Results.First();

                parentfiles.Files.Remove(deleteFileModel.FileName);

                WebApiConfig.GraphClient.Cypher
                        .Match("(parent:Folder)")
                        .Where((Folder parent) => parent.id == results.ParentId)
                        .Set("parent.ModificationDate = {modificationdate}")
                        .WithParam("modificationdate", DateTime.UtcNow)
                        .Set("parent.Files = {newFiles}")
                        .WithParam("newFiles", parentfiles.Files)
                        .ExecuteWithoutResults();
            }

            var service = this.DriveProvider.GetCredential();
            var request = service.Files.Delete(results.id);
            var fileToDelete = request.Execute();

            WebApiConfig.GraphClient.Cypher
                .Match("(file:File)")
                .Where((Models.File file) => file.FileName == deleteFileModel.FileName && file.ParentId == deleteFileModel.ParentId)
                .DetachDelete("file")
                .ExecuteWithoutResultsAsync();

            return Ok(results);
        }
    }
}