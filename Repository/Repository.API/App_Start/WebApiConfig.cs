﻿using System;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json.Serialization;
using System.Configuration;
using Neo4jClient;
using System.Net.Http.Formatting;

namespace Repository.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Rutas de Web API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{value}",
                defaults: new { value = RouteParameter.Optional }
            );

            // Para que WebAPI use JSON
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //Conectar a Neo4j
            var url = ConfigurationManager.AppSettings["GraphDBUrl"];
            var user = ConfigurationManager.AppSettings["GraphDBUser"];
            var password = ConfigurationManager.AppSettings["GraphDBPassword"];
            var client = new GraphClient(new Uri(url), user, password);
            client.Connect();

            GraphClient = client;
        }
        public static IGraphClient GraphClient { get; private set; }
    }
}